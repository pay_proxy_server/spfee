package com.qy.sp.fee.modules.piplecode.fish;

import com.qy.sp.fee.modules.piplecode.aofei.Aofei1Service;
import com.qy.sp.fee.modules.piplecode.aofei.Aofei2Service;
import com.qy.sp.fee.modules.piplecode.aofei.Aofei3Service;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/piple")
public class FishController {

	@Resource
	private FishService fishService;
	@Resource
	private Fish1Service fish1Service;


	@RequestMapping(value = "/fish/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(String smsId, String state, String moContent, String userid)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("smsId", smsId);
			object.put("state", state);
			object.put("moContent", moContent);
			object.put("userid", userid);
			fishService.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}

	@RequestMapping(value = "/fish/sync1" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest1(String smsId, String state, String moContent, String userid)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("smsId", smsId);
			object.put("state", state);
			object.put("moContent", moContent);
			object.put("userid", userid);
			fish1Service.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}

}
