package com.qy.sp.fee.modules.piplecode.ainipay;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class AinipayController {

	@Resource
	private AinipayService ainipayService;

	@RequestMapping(value = "/ainipay/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(String linkid, String mobile, String orderdest, String cmdid, String fee)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("linkid", linkid);
			object.put("mobile", mobile);
			object.put("orderdest", orderdest);
			object.put("cmdid", cmdid);
			object.put("fee", fee);
			ainipayService.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}

}
