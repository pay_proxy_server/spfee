package com.qy.sp.fee.modules.piplecode.nanchi;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.GlobalConst.DEC_STATUS;
import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.NumberUtil;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dto.TChannel;
import com.qy.sp.fee.dto.TChannelPiple;
import com.qy.sp.fee.dto.TChannelPipleKey;
import com.qy.sp.fee.dto.TOrder;
import com.qy.sp.fee.dto.TOrderExt;
import com.qy.sp.fee.dto.TOrderExtKey;
import com.qy.sp.fee.dto.TProduct;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;

import net.sf.json.JSONObject;
public class AofeiMonthService extends ChannelService{
	protected String getPrefix(){
		return "";
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		String userId = requestBody.optString("userId");
		String serviceId = requestBody.optString("serviceId");
		String webId  = requestBody.optString("webId");
		String area = requestBody.optString("area");
		String productCode = requestBody.optString("productCode");
		String data[] = webId.split("@");
		String apiKey = webId.substring(0, 4);
		String extData = webId.substring(4);
		TChannel tChannel = tChannelDao.selectByApiKey(apiKey);
		logger.info("checkInfo:"+requestBody.toString());
		JSONObject result = new JSONObject();
		if(tChannel == null){
			result.put("resultCode",GlobalConst.Result.ERROR);
			result.put("resultMsg","渠道号不存在");
			return result;
		}else{
			String orderId = getPrefix()+userId;
			TOrder order = tOrderDao.selectByPrimaryKey(orderId);
			if(order != null && order.getOrderStatus() ==GlobalConst.OrderStatus.SUCCESS){
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","订单已订购");
				return result;
			}
			String param = "?userId="+userId+"&serviceId="+serviceId+"&webId="+StringUtil.urlEncodeWithUtf8(webId)+"&area="+area;
			if("3028".equals(apiKey)){
				String resultCode = HttpClientUtils.doGet("http://121.199.251.172/osdk/qyIdoCheck.do"+param, HttpClientUtils.UTF8);
				logger.info("resultCode:"+resultCode);
				if(!"0000".equals(resultCode)){
					result.put("resultCode",GlobalConst.Result.ERROR);
					result.put("resultMsg","验证失败");
					return result;
				}
			}else if("3029".equals(apiKey)){
				String resultCode = HttpClientUtils.doGet("http://charge.51mrp.com/access-common/zhhyb-af-monthly/riskCheck.do"+param, HttpClientUtils.UTF8);
				logger.info("resultCode:"+resultCode);
				if(!"0000".equals(resultCode)){
					result.put("resultCode",GlobalConst.Result.ERROR);
					result.put("resultMsg","验证失败");
					return result;
				}
			}else if("1003".equals(apiKey)){
				
			}else if("3030".equals(apiKey)){
				String resultCode = HttpClientUtils.doGet("http://114.55.144.136:8220/spfee/piple/aofeibaoyue/check"+param, HttpClientUtils.UTF8);
				logger.info("resultCode:"+resultCode);
				if(!"0000".equals(resultCode)){
					result.put("resultCode",GlobalConst.Result.ERROR);
					result.put("resultMsg","验证失败");
					return result;
				}
			}else{
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","验证失败");
				return result;
			}
			if(order == null){
				order = new TOrder();
				order.setOrderId(orderId);
				TProduct tProduct = tProductDao.selectByCode(productCode);
				order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
				order.setChannelId(tChannel.getChannelId());
				order.setCreateTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
				order.setPipleId(getPipleId());
				order.setProductId(tProduct.getProductId());
				order.setSubStatus(GlobalConst.SubStatus.PAY_SEND_MESSAGE_SUCCESS);
				order.setGroupId(KeyHelper.createKey());
				order.setExtData(extData);
				statistics(STEP_BACK_SMS_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
				SaveOrderInsert(order);
				result.put("resultCode",GlobalConst.Result.SUCCESS);
				result.put("resultMsg","验证成功");
			}else{
				if(GlobalConst.SubStatus.PAY_ERROR_TG == order.getSubStatus()){
					result.put("resultCode",GlobalConst.Result.ERROR);
					result.put("resultMsg","用户退订");
					return result;
				}
				TProduct tProduct = tProductDao.selectByCode(productCode);
				order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
				order.setChannelId(tChannel.getChannelId());
				order.setCreateTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
				order.setPipleId(getPipleId());
				order.setProductId(tProduct.getProductId());
				order.setSubStatus(GlobalConst.SubStatus.PAY_SEND_MESSAGE_SUCCESS);
				order.setExtData(extData);
				statistics(STEP_BACK_SMS_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
				SaveOrderUpdate(order);
				result.put("resultCode",GlobalConst.Result.SUCCESS);
				result.put("resultMsg","验证成功");
			}
			statistics(STEP_BACK_SMS_BASE_TO_PLATFORM, order.getGroupId(),result.toString());
			return result;
		}
		
	}
	@Override
	public String processPaySuccess(JSONObject requestBody) throws Exception {
		logger.info("支付同步数据:"+requestBody);
		String error = "error";
		String serviceId = requestBody.optString("serviceId");
		String productId = requestBody.optString("productId");
		String traceUniqueId = requestBody.optString("traceUniqueId");
		String timeStamp  = requestBody.optString("timeStamp");
		String webId  = requestBody.optString("webId");
		String userId  = requestBody.optString("userId");
		String updateType  = requestBody.optString("updateType");
		String validate  = requestBody.optString("validate");
		TOrder existOrder = tOrderDao.selectByPrimaryKey(getPrefix()+userId);
		if(existOrder == null){
			return "error";
		}
		String channelId = "";
		TOrderExtKey tOrderExtKey = new TOrderExtKey();
		tOrderExtKey.setOrderId(existOrder.getOrderId());
		tOrderExtKey.setExtKey("originChannelId");
		TOrderExt orderExt = tOrderExtDao.selectByPrimaryKey(tOrderExtKey);
		if(orderExt != null){
			channelId = orderExt.getExtValue();
		}else{
			channelId = existOrder.getChannelId();
		}
		boolean isSend = false;
		TChannelPipleKey pkey = new TChannelPipleKey();
		pkey.setPipleId(getPipleId());
		pkey.setChannelId(channelId);
		TChannelPiple cp =  tChannelPipleDao.selectByPrimaryKey(pkey);
		if(cp == null)
			return "error";
		MonthOrder order = new MonthOrder();
		order.setTOrder(existOrder);
		if(orderExt != null){
			order.setOriginChannelId(orderExt.getExtValue());
		}
		if("1".equals(updateType) ){ // 同步数据正确
			if(order.getOrderStatus() == GlobalConst.OrderStatus.SUCCESS){
				return "ok";
			}
			order.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
			order.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS_DG);
			order.setModTime(DateTimeUtils.getCurrentTime());
			order.setCompleteTime(DateTimeUtils.getCurrentTime());
			order.setResultCode(updateType);
			order.setDgFlag(updateType);
			order.setBuyTime(timeStamp);
			order.setPipleOrderId(traceUniqueId);
			doWhenPaySuccess(order);
			boolean bDeducted  = order.deduct(cp.getVolt());
			if(!bDeducted){ 
				String cutChannelId = cp.getCutChannelId();
				if(StringUtil.isNotEmptyString(cutChannelId)){
					int cutPrecent = NumberUtil.getInteger(cp.getCutPercent());
					int rnd = KeyHelper.randomVal();
					boolean isCut = false;
					if(rnd <= cutPrecent){
						isCut = true;
					}
					if(isCut){
						String originChannelId = order.getChannelId();
						order.setChannelId(cp.getCutChannelId());
						order.setOriginChannelId(originChannelId);
					}else{
						isSend =true;
					}
					
				}else{
					isSend = true;
				}
				
			}
		}else{ 
			int decStatus = order.getDecStatus();
			if(DEC_STATUS.UNDEDUCTED == decStatus){
				String originChannelId = order.getOriginChannelId();
				if(StringUtil.isEmpty(originChannelId)){
					isSend =true;
				}
			}
			order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
			order.setSubStatus(GlobalConst.SubStatus.PAY_ERROR_TG);
			order.setModTime(DateTimeUtils.getCurrentTime());
			order.setCompleteTime(DateTimeUtils.getCurrentTime());
			order.setResultCode(updateType);
		}
		statistics(STEP_PAY_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
		SaveOrderUpdate(order);
		if(isSend){ // 不扣量 通知渠道
			try{
				TProduct tProduct = this.tProductDao.selectByPrimaryKey(order.getProductId());
				TChannel tChannel = this.tChannelDao.selectByPrimaryKey(order.getChannelId());
				String url = "%s?orderId=%s&pipleId=%s&productCode=%s&apiKey=%s&extData=%s&DGFlag=%s&DGTime=%s";
				url = StringUtil.format(url, cp.getNotifyUrl(),order.getOrderId(),order.getPipleId(),tProduct.getProductCode(),tChannel.getApiKey(),order.getExtData(),updateType,timeStamp);
				statistics(STEP_PAY_PLATFORM_TO_CHANNEL, order.getGroupId(),url);
				String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
				statistics(STEP_PAY_CHANNEL_TO_PLATFORM, order.getGroupId(),result);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return "ok";
	}
	
	public static class MonthOrder extends TOrder{
		private String buyTime;
		private String dgFlag;
		private String originChannelId;
		
		public String getBuyTime() {
			return buyTime;
		}

		public void setBuyTime(String buyTime) {
			this.buyTime = buyTime;
		}

		public String getDgFlag() {
			return dgFlag;
		}

		public void setDgFlag(String dgFlag) {
			this.dgFlag = dgFlag;
		}

		public String getOriginChannelId() {
			return originChannelId;
		}

		public void setOriginChannelId(String originChannelId) {
			this.originChannelId = originChannelId;
		}

		public List<TOrderExt> gettOrderExts() {
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.buyTime != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("buyTime");
				oExt.setExtValue(this.buyTime);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.dgFlag != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("dgFlag");
				oExt.setExtValue(this.dgFlag);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(StringUtil.isNotEmptyString(originChannelId)){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("originChannelId");
				oExt.setExtValue(this.originChannelId);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}
	/**
	 provinces = new HashMap<String, String>();
     provinces.put("230", "重庆");
     provinces.put("290", "陕西");
     provinces.put("351", "山西");
     provinces.put("100", "北京");
     provinces.put("551", "安徽");
     provinces.put("571", "浙江");
     provinces.put("311", "河北");
     provinces.put("280", "四川");
     provinces.put("951", "宁夏");
     provinces.put("240", "辽宁");
     provinces.put("871", "云南");
     provinces.put("471", "内蒙古");
     provinces.put("851", "贵州");
     provinces.put("791", "江西");
     provinces.put("898", "海南");
     provinces.put("451", "黑龙江");
     provinces.put("591", "福建");
     provinces.put("210", "上海");
     provinces.put("200", "广东");
     provinces.put("371", "河南");
     provinces.put("971", "青海");
     provinces.put("731", "湖南");
     provinces.put("531", "山东");
     provinces.put("991", "新疆");
     provinces.put("771", "广西");
     provinces.put("931", "甘肃");
     provinces.put("431", "吉林");
     provinces.put("250", "江苏");
     provinces.put("270", "湖北");
     provinces.put("220", "天津");
     provinces.put("891", "西藏");
     **/
}
