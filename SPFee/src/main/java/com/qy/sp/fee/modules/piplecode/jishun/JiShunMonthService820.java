package com.qy.sp.fee.modules.piplecode.jishun;

import org.springframework.stereotype.Service;
@Service
public class JiShunMonthService820 extends JiShunMonthService{
	@Override
	public String getPipleId() {
		return "14741687457395468019725";
	}
	protected String getPrefix(){
		return "820_";
	}
}
