package com.qy.sp.fee.modules.piplecode.jishun;

import org.springframework.stereotype.Service;
@Service
public class JiShunMonthService814 extends JiShunMonthService{
	@Override
	public String getPipleId() {
		return "14741686027800475508942";
	}
	protected String getPrefix(){
		return "814_";
	}
}
