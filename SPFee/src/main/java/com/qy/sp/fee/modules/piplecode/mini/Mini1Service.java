package com.qy.sp.fee.modules.piplecode.mini;

import com.qy.sp.fee.common.utils.*;
import com.qy.sp.fee.dto.*;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class Mini1Service extends ChannelService {
	private final static String PIPLE_ID = "14811811411512783478731";
	private final static String PRODUCT_CODE = "P01500";
	private final static String APP_ID = "21842563";
	private final static String CP_ID = "11151";

	private final static String PAY_SUCCESS = "0000";

	private static Map<String,String> provinceMap = new ConcurrentHashMap<String, String>();
	private static Map<String,String> errorMap = new ConcurrentHashMap<String, String>();

	private Logger log = LoggerFactory.getLogger(Mini1Service.class);

	@Override
	protected boolean isUseableTradeDayAndMonth() {
		return true;
	}

	@Override
	public String getPipleId() {
		return PIPLE_ID;
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		log.info("Mini1Service processGetSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String hostIp = requestBody.optString("hostIp");
		String imsi = requestBody.optString("imsi");
		String imei = requestBody.optString("imei");
		String ipProvince = requestBody.optString("ipProvince");
		String extData = requestBody.optString("extData");
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey)   || StringUtil.isEmpty(pipleId)  || StringUtil.isEmpty(productCode) || StringUtil.isEmpty(imsi))
		{
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		}
		else
		{
			BaseChannelRequest req = new BaseChannelRequest();
			req.setApiKey(apiKey);
			req.setImsi(imsi);
			req.setProductCode(productCode);
			req.setMobile(mobile);
			req.setIpProvince(ipProvince);
			// 调用合法性校验
			BaseResult bResult = this.accessVerify(req,pipleId);
			// 返回不为空则校验不通过
			if(bResult!=null)
			{
				result.put("resultCode",bResult.getResultCode());
				result.put("resultMsg",bResult.getResultMsg());
				return result;
			}
			String groupId = KeyHelper.createKey();
			statistics(STEP_GET_SMS_CHANNEL_TO_PLATFORM, groupId, requestBody.toString());
			TChannel tChannel = tChannelDao.selectByApiKey(req.getApiKey());
			TProduct tProduct = tProductDao.selectByCode(req.getProductCode());
			TPipleProductKey ppkey = new TPipleProductKey();
			ppkey.setPipleId(pipleId);
			ppkey.setProductId(tProduct.getProductId());
			TPiple piple = tPipleDao.selectByPrimaryKey(pipleId);
			//保存订单
			Mini1Service.MiniOrder order = new Mini1Service.MiniOrder();
			order.setOrderId(KeyHelper.createKey());
			order.setPipleId(pipleId);
			order.setChannelId(tChannel.getChannelId());
			order.setMobile(mobile);
			order.setImsi(imsi);
			order.setImei(imei);
			order.setProvinceId(req.getProvinceId());
			order.setProductId(tProduct.getProductId());
			order.setOrderStatus(GlobalConst.OrderStatus.INIT);
			order.setSubStatus(GlobalConst.SubStatus.PAY_INIT);
			order.setCreateTime(DateTimeUtils.getCurrentTime());
			order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
			order.setGroupId(groupId);
			order.setExtData(extData);
			try
			{
				SaveOrderInsert(order);
				result.put("orderId",order.getOrderId());

				Map<String, String> params = new HashMap<String, String>();
				THost host = tHostDao.selectByPrimaryKey(req.getHostId());
				if(host != null)
				{
					if("中国移动".equals(host.getHostName()))
						params.put("provider", "1");
					else if("中国联通".equals(host.getHostName()))
						params.put("provider", "2");
					else if("中国电信".equals(host.getHostName()))
						params.put("provider", "3");
					params.put("appid", APP_ID);
					params.put("cpid", CP_ID);
					params.put("price", "1500");
					params.put("timestamp", (DateTimeUtils.getCurrentTime().getTime() - new GregorianCalendar(1970, 0, 1).getTimeInMillis()) + "");
					params.put("phonenum", mobile);
					params.put("imsi", imsi);
					params.put("imei", imei);
					params.put("userip", hostIp);
					params.put("cporderno", order.getOrderId());
					params.put("channelcode", order.getChannelId());
					TProvince province = tProvinceDao.selectByPrimaryKey(req.getProvinceId());
					if(province != null)
					{
						if(provinceMap.containsKey(province.getProvinceName()))
						{
							params.put("province", provinceMap.get(province.getProvinceName()));
							log.info(" MiniService reqUrl:"+  piple.getPipleUrlA()+";"+params.toString());
							statistics(STEP_GET_SMS_PLATFORM_TO_BASE, groupId,piple.getPipleUrlA()+";"+params.toString());
							String pipleResult= HttpClientUtils.doPost(piple.getPipleUrlA(), params, HttpClientUtils.UTF8);
							log.info(" MiniService getPipleResult:"+  pipleResult);
							statistics(STEP_BACK_SMS_BASE_TO_PLATFORM, groupId,pipleResult);

							if(pipleResult != null && !"".equals(pipleResult))
							{
								JSONObject jsonObj = JSONObject.fromObject(pipleResult);
								if(jsonObj.has("code") )
								{
									String resultCode = jsonObj.optString("code");
									if(SUCCESS_CODE.equals(resultCode))
									{
										JSONArray resultArray = jsonObj.getJSONArray("array");
										for(int i=0;i<resultArray.size();i++)
										{
											JSONObject json = resultArray.getJSONObject(i);
											if("sendMsgL".equals(json.optString("type")) || "sendMsg".equals(json.optString("type")))
											{
												result.put("smsContent1", json.optString("smsContent"));
												result.put("smsSend1", json.optString("smsSend"));
											}
											else if("wait".equals(json.optString("type")))
											{
												result.put("waitTime", json.optString("waitTime"));
											}
											else if("rys".equals(json.optString("type")))
											{
												result.put("smsContent2", json.optString("smsContent"));
												result.put("smsSend2", json.optString("smsSend"));
											}
										}
										if(result.containsKey("smsSend2") && "".equals(result.optString("smsSend2")) && result.containsKey("smsSend1"))
											result.put("smsSend2", result.optString("smsSend1"));
										result.put("resultCode", GlobalConst.Result.SUCCESS);
										result.put("resultMsg","请求成功。");

										order.setResultCode(resultCode);
										order.setPipleResultContent(errorMap.get(resultCode));
										order.setSmsContent1(StringUtil.urlEncodeWithUtf8(result.optString("smsContent1")));
										order.setSmsContent2(StringUtil.urlEncodeWithUtf8(result.optString("smsContent2")));
										order.setSmsSend1(result.optString("smsSend1"));
										order.setSmsSend2(result.optString("smsSend2"));
										order.setWaitTime(result.optString("waitTime"));
										order.setModTime(DateTimeUtils.getCurrentTime());
										order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
										order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_SUCCESS);
									}
									else
									{
										result.put("resultCode", GlobalConst.Result.ERROR);
										result.put("resultMsg","请求失败:" + resultCode);
										order.setResultCode(resultCode);
										order.setPipleResultContent(errorMap.get(resultCode));
										order.setModTime(DateTimeUtils.getCurrentTime());
										order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
										order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
									}
								}
								else
								{
									result.put("resultCode", GlobalConst.Result.ERROR);
									result.put("resultMsg","请求失败:" + pipleResult);
									order.setPipleResultContent("返回值中没有状态码");
									order.setModTime(DateTimeUtils.getCurrentTime());
									order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
									order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
								}
							}
							else
							{
								result.put("resultCode", GlobalConst.Result.ERROR);
								result.put("resultMsg","请求失败");
								order.setPipleResultContent("请求没有返回值");
								order.setModTime(DateTimeUtils.getCurrentTime());
								order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
								order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
							}
						}
						else
						{
							result.put("resultCode", GlobalConst.Result.ERROR);
							result.put("resultMsg","请求失败");
							order.setPipleResultContent("电话号码所对应的省份不在省份列表中");
							order.setModTime(DateTimeUtils.getCurrentTime());
							order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
							order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
						}
					}
					else
					{
						result.put("resultCode", GlobalConst.Result.ERROR);
						result.put("resultMsg","请求失败");
						order.setPipleResultContent("通过电话号码获得省份信息失败");
						order.setModTime(DateTimeUtils.getCurrentTime());
						order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
						order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
					}
				}
				else
				{
					result.put("resultCode", GlobalConst.Result.ERROR);
					result.put("resultMsg","请求失败");
					order.setPipleResultContent("通过电话号码获得运营商信息失败");
					order.setModTime(DateTimeUtils.getCurrentTime());
					order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
					order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","请求失败，接口异常:"+e.getMessage());
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
			}
			SaveOrderUpdate(order);
			statistics(STEP_BACK_SMS_PLATFORM_TO_CHANNEL, groupId, result.toString());
			return result;
		}
	}



	public class MiniOrder extends TOrder{
		private String pipleResultContent;
		private String smsContent1;
		private String smsSend1;
		private String smsContent2;
		private String smsSend2;
		private String waitTime;

		public String getPipleResultContent() {
			return pipleResultContent;
		}

		public void setPipleResultContent(String pipleResultContent) {
			this.pipleResultContent = pipleResultContent;
		}

		public String getSmsContent1() {
			return smsContent1;
		}

		public void setSmsContent1(String smsContent1) {
			this.smsContent1 = smsContent1;
		}

		public String getSmsSend1() {
			return smsSend1;
		}

		public void setSmsSend1(String smsSend1) {
			this.smsSend1 = smsSend1;
		}

		public String getSmsContent2() {
			return smsContent2;
		}

		public void setSmsContent2(String smsContent2) {
			this.smsContent2 = smsContent2;
		}

		public String getSmsSend2() {
			return smsSend2;
		}

		public void setSmsSend2(String smsSend2) {
			this.smsSend2 = smsSend2;
		}

		public String getWaitTime() {
			return waitTime;
		}

		public void setWaitTime(String waitTime) {
			this.waitTime = waitTime;
		}

		public List<TOrderExt> gettOrderExts()
		{
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.pipleResultContent != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("pipleResultContent");
				oExt.setExtValue(this.pipleResultContent);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsContent1 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent1");
				oExt.setExtValue(this.smsContent1);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsContent2 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent2");
				oExt.setExtValue(this.smsContent2);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend1 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend1");
				oExt.setExtValue(this.smsSend1);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend2 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend2");
				oExt.setExtValue(this.smsSend2);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.waitTime != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("waitTime");
				oExt.setExtValue(this.waitTime);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}

	static {
		provinceMap.put("安徽省", "AH");
		provinceMap.put("北京市", "BJ");
		provinceMap.put("福建省", "FJ");
		provinceMap.put("甘肃省", "GS");
		provinceMap.put("广东省", "GD");
		provinceMap.put("广西壮族自治区", "GX");
		provinceMap.put("贵州省", "GZ");
		provinceMap.put("海南省", "HI");
		provinceMap.put("河北省", "HE");
		provinceMap.put("河南省", "HA");
		provinceMap.put("黑龙江省", "HL");
		provinceMap.put("湖北省", "HB");
		provinceMap.put("湖南省", "HN");
		provinceMap.put("吉林省", "JL");
		provinceMap.put("江苏省", "JS");
		provinceMap.put("江西省", "JX");
		provinceMap.put("辽宁省", "LN");
		provinceMap.put("内蒙古自治区", "NM");
		provinceMap.put("宁夏回族自治区", "NX");
		provinceMap.put("青海省", "QH");
		provinceMap.put("山东省", "SD");
		provinceMap.put("山西省", "SX");
		provinceMap.put("上海市", "SH");
		provinceMap.put("四川省", "SC");
		provinceMap.put("陕西省", "SN");
		provinceMap.put("天津市", "TJ");
		provinceMap.put("西藏自治区", "XZ");
		provinceMap.put("新疆维吾尔自治区", "XJ");
		provinceMap.put("云南省", "YN");
		provinceMap.put("浙江省", "ZJ");
		provinceMap.put("重庆市", "CQ");
	}

	private final static String ERROR_CODE1 = "-1";
	private final static String ERROR_CODE2 = "-10";
	private final static String ERROR_CODE3 = "6001";
	private final static String ERROR_CODE4 = "11";
	private final static String ERROR_CODE5 = "-7";
	private final static String ERROR_CODE6 = "13";
	private final static String ERROR_CODE7 = "28";
	private final static String ERROR_CODE8 = "27";
	private final static String ERROR_CODE9 = "26";
	private final static String ERROR_CODE10 = "25";
	private final static String ERROR_CODE11 = "24";
	private final static String ERROR_CODE12 = "20";
	private final static String ERROR_CODE13 = "21";
	private final static String ERROR_CODE14 = "22";
	private final static String ERROR_CODE15 = "23";
	private final static String ERROR_CODE16 = "61";
	private final static String ERROR_CODE17 = "62";
	private final static String ERROR_CODE18 = "63";
	private final static String ERROR_CODE19 = "64";
	private final static String SUCCESS_CODE = "00000";

	static {
		errorMap.put(ERROR_CODE1, "缺少provider,appid,cpid,price中其一");
		errorMap.put(ERROR_CODE2, "缺少province");
		errorMap.put(ERROR_CODE3, "appid不存在");
		errorMap.put(ERROR_CODE4, "无通道配置");
		errorMap.put(ERROR_CODE5, "单个用户请求太频繁");
		errorMap.put(ERROR_CODE6, "没有该资费可用");
		errorMap.put(ERROR_CODE7, "通道无该金额");
		errorMap.put(ERROR_CODE8, "运营商不正确");
		errorMap.put(ERROR_CODE9, "通道需要手机号码");
		errorMap.put(ERROR_CODE10, "业务屏蔽时间段");
		errorMap.put(ERROR_CODE11, "通道省份未开通");
		errorMap.put(ERROR_CODE12, "缺少通道必须字段");
		errorMap.put(ERROR_CODE13, "运营省份未开通");
		errorMap.put(ERROR_CODE14, "通道省份未开通");
		errorMap.put(ERROR_CODE15, "资费省份未开通");
		errorMap.put(ERROR_CODE16, "日限金额");
		errorMap.put(ERROR_CODE17, "月限金额");
		errorMap.put(ERROR_CODE18, "日限次数");
		errorMap.put(ERROR_CODE19, "月限次数");
		errorMap.put(SUCCESS_CODE, "请求成功");
	}

}
