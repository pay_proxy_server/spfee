package com.qy.sp.fee.modules.piplecode.bwhite;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qy.sp.fee.common.utils.Base64;
import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.NumberUtil;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dao.TSdkConfigDao;
import com.qy.sp.fee.dao.TSdkconfigMobileBaseDao;
import com.qy.sp.fee.dto.TChannel;
import com.qy.sp.fee.dto.TOrder;
import com.qy.sp.fee.dto.TPiple;
import com.qy.sp.fee.dto.TProduct;
import com.qy.sp.fee.dto.TProvince;
import com.qy.sp.fee.dto.TSdkConfig;
import com.qy.sp.fee.dto.TSdkConfigQueryKey;
import com.qy.sp.fee.dto.TSdkconfigMobileBase;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.apisdk.service.ApiSdkConfigService;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;

import net.sf.json.JSONObject;

@Service
public class BWhiteService extends ChannelService {

	public static final String CONFIG_CODE_START_TIME ="codeStartTime";
	public static final String CONFIG_CODE_ISBWHITE ="isBWhite";
	public static final String CONFIG_CODE_IS_SHOW_NORMAL ="isShowBWNormal";
	@Resource
	private TSdkConfigDao tSdkConfigDao;
	@Resource
	private TSdkconfigMobileBaseDao tSdkconfigMobileBaseDao;
	@Resource
	private ApiSdkConfigService apiSdkConfigService;
	@Override
	public String getPipleId() {
		return "14714136609207184697301";
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String imsi = requestBody.optString("imsi");
		String imei = requestBody.optString("imei");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String appId = requestBody.optString("appId");
		String extData = requestBody.optString("extData");
		String pipleOrderId = requestBody.optString("pipleOrderId");
		String contentId = requestBody.optString("contentId");
		String releaseChannelId = requestBody.optString("releaseChannelId");
		String cpId = requestBody.optString("cpId");
		String cId = requestBody.optString("cid");
		String appVersion = requestBody.optString("appVersion");
		String ipProvince = requestBody.optString("ipProvince");
		JSONObject result = new JSONObject();
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey) || StringUtil.isEmptyString(mobile)|| StringUtil.isEmptyString(appId) ){
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		} 
		BaseChannelRequest req = new BaseChannelRequest();
		req.setApiKey(apiKey);
		req.setProductCode(productCode);
		req.setPipleId(pipleId);
		req.setMobile(mobile);
		req.setImsi(imsi);
		req.setIpProvince(ipProvince);
		boolean isCreateOrder = false;
		BaseResult bResult = this.accessVerify(req);
		TChannel tChannel = this.tChannelDao.selectByApiKey(apiKey);
		if (bResult != null) {// 返回不为空则校验不通过
			result.put("resultCode",bResult.getResultCode());
			result.put("resultMsg",bResult.getResultMsg());
			if(StringUtil.equals(GlobalConst.CheckResult.UNKNOWN_APIKEY+"",bResult.getResultCode())
				|| StringUtil.equals(GlobalConst.CheckResult.PIPLE_ERROR+"",bResult.getResultCode())
				|| StringUtil.equals(GlobalConst.CheckResult.PRODUCTCODE_ERROR+"",bResult.getResultCode())
			){
				return result;
			}else{
				if(req.getProvinceId() == 0){
					req.setProvinceId(mobileSegmentService.getProvinceIdByMobile(mobile));
				}
				if(req.getProvinceId() == 0){
					req.setProvinceId(mobileSegmentService.getProvinceByIpProvince(ipProvince));
				}
			}
		}else{ 
			result= checkBwhite(appId, contentId, releaseChannelId, cpId, cId, appVersion, ipProvince, req, tChannel);
		}
		String resultCode = result.optString("resultCode");
		if(StringUtil.isNotEmptyString(resultCode)){
			TSdkConfigQueryKey isSHowNormalConfigKey = new TSdkConfigQueryKey();
			isSHowNormalConfigKey.setAppId(appId);
			isSHowNormalConfigKey.setChannelId(tChannel.getChannelId());
			isSHowNormalConfigKey.setPipleId(getPipleId());
			isSHowNormalConfigKey.setProvinceId(req.getProvinceId()+"");
			isSHowNormalConfigKey.setConfigId(CONFIG_CODE_IS_SHOW_NORMAL);
			TSdkConfig isSHowNormalConfig = apiSdkConfigService.queryConfiguration(isSHowNormalConfigKey);
			if(isSHowNormalConfig != null){
				isCreateOrder = NumberUtil.getBoolean(isSHowNormalConfig.getConfigValue());
			}
		}else{
			result.put("resultCode",GlobalConst.Result.SUCCESS);
			result.put("resultMsg","获取成功");
			isCreateOrder = true;
		}
		
		if(isCreateOrder){
			String groupId = KeyHelper.createKey();
			TProduct tProduct = this.tProductDao.selectByCode(productCode);
			statistics( STEP_GET_SMS_CHANNEL_TO_PLATFORM, groupId, requestBody.toString());
			TOrder order = new TOrder();
			order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
			order.setChannelId(tChannel.getChannelId());
			order.setCreateTime(DateTimeUtils.getCurrentTime());
			order.setImei(imei);
			order.setImsi(imsi);
			order.setMobile(mobile);
			order.setOrderId(KeyHelper.createKey());
			order.setOrderStatus(GlobalConst.OrderStatus.INIT);
			order.setPipleId(getPipleId());
			order.setPipleOrderId(pipleOrderId);
			order.setProductId(tProduct.getProductId());
			order.setProvinceId(req.getProvinceId());
			order.setSubStatus(GlobalConst.SubStatus.PAY_INIT);
			order.setGroupId(groupId);
			order.setExtData(extData);
			order.setAppId(appId);
			this.SaveOrderInsert(order);
			result.put("orderId",order.getOrderId());
			statistics(STEP_BACK_SMS_PLATFORM_TO_CHANNEL, groupId, JSONObject.fromObject(result).toString());
		}
		return result;
	}
	private JSONObject checkBwhite(String appId, String contentId, String releaseChannelId, String cpId, String cId,
			String appVersion, String ipProvince, BaseChannelRequest req, TChannel tChannel)
			throws Exception {
		JSONObject result = new JSONObject();
		TSdkConfigQueryKey startTimekey = new TSdkConfigQueryKey();
		startTimekey.setAppId(appId);
		startTimekey.setChannelId(tChannel.getChannelId());
		startTimekey.setPipleId(getPipleId());
		startTimekey.setProvinceId(req.getProvinceId()+"");
		startTimekey.setConfigId(CONFIG_CODE_START_TIME);
		TSdkConfig startTimeConfig = apiSdkConfigService.queryConfiguration(startTimekey);
		if(startTimeConfig != null){
			try{
				String codeStartTime = startTimeConfig.getConfigValue();
				Date currentDate = DateTimeUtils.getCurrentTime();
				currentDate.setYear(70);
				currentDate.setMonth(0);
				currentDate.setDate(1);
				String times[] = codeStartTime.split(",");
				boolean isTimeOk = false;
				for(String time: times){
					time = time.substring(1,time.length()-1);
					String start = time.split("-")[0];
					Date startDate = DateTimeUtils.toTime(start,"HH:mm:ss");
					String end = time.split("-")[1];
					Date endDate =DateTimeUtils.toTime(end,"HH:mm:ss");
					if(currentDate.getTime() > startDate.getTime() && currentDate.getTime() < endDate.getTime()){
						isTimeOk = true;
					}
				}
				if(!isTimeOk){
					result.put("resultCode","-1");
					result.put("resultMsg","不在全局时间段内，请联系管理员");
					return result;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		TSdkConfigQueryKey isBWhitekey = new TSdkConfigQueryKey();
		isBWhitekey.setAppId(appId);
		isBWhitekey.setChannelId(tChannel.getChannelId());
		isBWhitekey.setPipleId(getPipleId());
		isBWhitekey.setProvinceId(req.getProvinceId()+"");
		isBWhitekey.setConfigId(CONFIG_CODE_ISBWHITE);
		TSdkConfig isBWhiteConfig = apiSdkConfigService.queryConfiguration(isBWhitekey);
		if(isBWhiteConfig != null){
			boolean isBWhite = NumberUtil.getBoolean(isBWhiteConfig.getConfigValue()); 
			if(!isBWhite){
				result.put("resultCode","-1");
				result.put("resultMsg","全局关闭，请联系管理员");
				return result;
			}
		}
		TSdkconfigMobileBase tSdkconfigMobileBase = new TSdkconfigMobileBase();
		tSdkconfigMobileBase.setAppId(appId);
		tSdkconfigMobileBase.setContentId(contentId);
		tSdkconfigMobileBase.setCpId(cpId);
		tSdkconfigMobileBase.setReleaseChannelId(releaseChannelId);
		tSdkconfigMobileBase.setcId(cId);
		tSdkconfigMobileBase.setProvinceId(req.getProvinceId());
		tSdkconfigMobileBase.setAppVersion(appVersion);
		TSdkconfigMobileBase sdkconfigMobileConfig = tSdkconfigMobileBaseDao.selectSelective(tSdkconfigMobileBase);
		if(sdkconfigMobileConfig != null){
			boolean isOPen = NumberUtil.getBoolean(sdkconfigMobileConfig.getIsOpen());
			if(!isOPen){
				result.put("resultCode","-1");
				result.put("resultMsg","基地该渠道未打开，联系管理员");
				return result;
			}
			boolean isBWhite = NumberUtil.getBoolean(sdkconfigMobileConfig.getIsUseBWhite());
			if(!isBWhite){
				result.put("resultCode","-1");
				result.put("resultMsg","基地该渠道黑白包未开启");
				return result;
			}
			String startCodeTime = sdkconfigMobileConfig.getStartCodeTime();
			if(StringUtil.isNotEmptyString(startCodeTime)){
				Date currentDate = DateTimeUtils.getCurrentTime();
				currentDate.setYear(70);
				currentDate.setMonth(0);
				currentDate.setDate(1);
				String times[] = startCodeTime.split(",");
				boolean isTimeOk = false;
				for(String time: times){
					time = time.substring(1,time.length()-1);
					String start = time.split("-")[0];
					Date startDate = DateTimeUtils.toTime(start,"HH:mm:ss");
					String end = time.split("-")[1];
					Date endDate =DateTimeUtils.toTime(end,"HH:mm:ss");
					if(currentDate.getTime() > startDate.getTime() && currentDate.getTime() < endDate.getTime()){
						isTimeOk = true;
					}
				}
				if(!isTimeOk){
					result.put("resultCode","-1");
					result.put("resultMsg","该基地渠道开放,不在时间段内");
					return result;
				}
			}else{
				result.put("resultCode","-1");
				result.put("resultMsg","该省份时间段未开通。请开通");
				return result;
			}
			boolean isIpProvince = NumberUtil.getBoolean(sdkconfigMobileConfig.getIsIpProvince(),true);
			if(isIpProvince){
				if(StringUtil.isEmpty(ipProvince)){
					result.put("resultCode","-1");
					result.put("resultMsg","客户端省份未知");
					return result;
				}
				TProvince province = tProvinceDao.selectByProvinceName(ipProvince);
				if(province == null || province.getProvinceId() != req.getProvinceId()){
					result.put("resultCode","-1");
					result.put("resultMsg","号码省份和IP省份不一致");
					return result;
				}
			}
		}else{
			result.put("resultCode","-1");
			result.put("resultMsg","该版本没有配置策略。");
			return result;
		}
		return result;
	}
	@Override
	protected boolean isUseableTradeDayAndMonth() {
		return true;
	}
	public void updateOrderSuccess(JSONObject requestObject){
		String orderId = requestObject.optString("orderId");
		if(StringUtil.isNotEmptyString(orderId)){
			TOrder order = tOrderDao.selectByPrimaryKey(orderId);
			if(order != null && StringUtil.equals(order.getPipleId(), getPipleId())){
				order.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
				order.setCompleteTime(DateTimeUtils.getCurrentTime());
				tOrderDao.updateByPrimaryKeySelective(order);		
				statistics(STEP_PAY_CHANNEL_TO_PLATFORM, order.getGroupId(), "渠道设置订单成功："+orderId);
			}
		}
		
	}
	//http://180.101.227.228/vcode/recognize_bw
	public String processCheckCode(byte[] bytes){
		try{
			TPiple piple = tPipleDao.selectByPrimaryKey(getPipleId());
			String codeUrl = piple.getPipleUrlA();
			Map<String,String> param = new HashMap<String,String>();
			param.put("fileString", Base64.encodeBytes(bytes));
			String result = HttpClientUtils.doPost(codeUrl, param, HttpClientUtils.UTF8);
			if(StringUtil.isNotEmptyString(result) && result.length() ==4){
				return result;
			}
	        return "";
		 }catch(Exception e){
			e.printStackTrace(); 
		 }
		return "";
		
	}
	/*public String processCheckCode(byte[] bytes){
		try{
			 TessBaseAPI api = new TessBaseAPI();
	         if (api.Init(ClientProperty.getProperty("config", "TESSERACT_OCR_PATH"), "fontqy") != 0) {
	            logger.error("Could not initialize tesseract.");
	            return null;
	         }
	         PIX image = lept.pixReadMem(bytes, bytes.length);
	         if(image == null)
	        	return null;
	         api.SetImage(image);
	         BytePointer outText = api.GetUTF8Text();
	         if(outText == null){
	        	return null;
	         }
	         String text = outText.getString().trim().replace(" ", "");
	         logger.info("OCR output:\n" + text);
	         api.End();
	         outText.deallocate();
	         lept.pixDestroy(image);
	         return text;
		 }catch(Exception e){
			e.printStackTrace(); 
		 }
		return null;
		
	}*/
public static BufferedImage filterDeleteGrey(BufferedImage img) {
		
		int x, y;
		int r1[][] = new int[3][3];
		int g1[][] = new int[3][3];
		int b1[][] = new int[3][3];
		
		final int w = img.getWidth();
		final int h = img.getHeight();
		final BufferedImage dst = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Map<String, Integer> map = new HashMap();
		
		/* Remove white lines */
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				int colorCenter = img.getRGB(x, y);
				int color00 ;
				int color01 ;
				int color02 ;
				int color10 ;
				int color11 ;
				int color12 ;
				int color20 ;
				int color21 ;
				int color22 ;
				try{
					color00 = img.getRGB(x-1, y-1);
				}catch(Exception e){
					color00 = colorCenter;
				}
				try{
					color01 = img.getRGB(x-1, y);
				}catch(Exception e){
					color01 = colorCenter;
				}
				try{
				color02 = img.getRGB(x-1, y+1);
				}catch(Exception e){
					color02 = colorCenter;
				}
				try{
				color10 = img.getRGB(x, y-1);
				}catch(Exception e){
					color10 = colorCenter;
				}
				try{
				color11 = img.getRGB(x, y);
				}catch(Exception e){
					color11 = colorCenter;
				}
				try{
				color12 = img.getRGB(x, y+1);
				}catch(Exception e){
					color12 = colorCenter;
				}
				try{
				color20 = img.getRGB(x+1, y-1);
				}catch(Exception e){
					color20 = colorCenter;
				}
				try{
				color21 = img.getRGB(x+1, y);
				}catch(Exception e){
					color21 = colorCenter;
				}
				try{
				color22 = img.getRGB(x+1, y+1);
				}catch(Exception e){
					color22 = colorCenter;
				}
				
				dst.setRGB(x, y, colorCenter);
				r1[0][0] = (color00 >> 16) & 0xff;
				g1[0][0] = (color00 >> 8) & 0xff;
				b1[0][0] = (color00 & 0xff);
				
				r1[0][1] = (color01 >> 16) & 0xff;
				g1[0][1] = (color01 >> 8) & 0xff;
				b1[0][1] = (color01 & 0xff);
				
				r1[0][2] = (color02 >> 16) & 0xff;
				g1[0][2] = (color02 >> 8) & 0xff;
				b1[0][2] = (color02 & 0xff);
				
				r1[1][0] = (color10 >> 16) & 0xff;
				g1[1][0] = (color10 >> 8) & 0xff;
				b1[1][0] = (color10 & 0xff);
				
				
				r1[1][1] = (color11 >> 16) & 0xff;
				g1[1][1] = (color11 >> 8) & 0xff;
				b1[1][1] = (color11 & 0xff);
				
				
				r1[1][2] = (color12 >> 16) & 0xff;
				g1[1][2] = (color12 >> 8) & 0xff;
				b1[1][2] = (color12 & 0xff);
				
				r1[2][0] = (color20 >> 16) & 0xff;
				g1[2][0] = (color20 >> 8) & 0xff;
				b1[2][0] = (color20 & 0xff);
				
				r1[2][1] = (color21 >> 16) & 0xff;
				g1[2][1] = (color21 >> 8) & 0xff;
				b1[2][1] = (color21 & 0xff);
				
				r1[2][2] = (color22 >> 16) & 0xff;
				g1[2][2] = (color22 >> 8) & 0xff;
				b1[2][2] = (color22 & 0xff);
				boolean isBlack = false;
				for(int i=0 ;i <=2 ; i++){
					boolean  isBreak = false;
					for(int j=0 ; j<=2;j++){
						if(((255-r1[i][j])>160) && ((255-g1[i][j])>160) && ((255-b1[i][j])>160)){
							isBlack = true;
							break;
						}
					}
					if(isBreak){
						break;
					}
				}
				if(isBlack){
					if(((255-r1[1][1])>160) && ((255-g1[1][1])>160) && ((255-b1[1][1])>160)){
//						dst.setRGB(x, y, 0xffffff);
					}else{
						int sum = r1[1][1]+g1[1][1]+b1[1][1];
							int average = sum/3;
							if(Math.abs(average - r1[1][1]) <10 && Math.abs(average - g1[1][1]) <10 && Math.abs(average - b1[1][1]) <10){
								dst.setRGB(x, y, 0xffffff);
							}
								
						}
				}
			}
		}
		return dst;
	}
	
public static BufferedImage filterDeleteBlack(BufferedImage img) {
		
		int x, y;
		int r1[][] = new int[3][3];
		int g1[][] = new int[3][3];
		int b1[][] = new int[3][3];
		
		final int w = img.getWidth();
		final int h = img.getHeight();
		final BufferedImage dst = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Map<String, Integer> map = new HashMap();
		
		/* Remove white lines */
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				int colorCenter = img.getRGB(x, y);
				int color00 ;
				int color01 ;
				int color02 ;
				int color10 ;
				int color11 ;
				int color12 ;
				int color20 ;
				int color21 ;
				int color22 ;
				try{
					color00 = img.getRGB(x-1, y-1);
				}catch(Exception e){
					color00 = colorCenter;
				}
				try{
					color01 = img.getRGB(x-1, y);
				}catch(Exception e){
					color01 = colorCenter;
				}
				try{
				color02 = img.getRGB(x-1, y+1);
				}catch(Exception e){
					color02 = colorCenter;
				}
				try{
				color10 = img.getRGB(x, y-1);
				}catch(Exception e){
					color10 = colorCenter;
				}
				try{
				color11 = img.getRGB(x, y);
				}catch(Exception e){
					color11 = colorCenter;
				}
				try{
				color12 = img.getRGB(x, y+1);
				}catch(Exception e){
					color12 = colorCenter;
				}
				try{
				color20 = img.getRGB(x+1, y-1);
				}catch(Exception e){
					color20 = colorCenter;
				}
				try{
				color21 = img.getRGB(x+1, y);
				}catch(Exception e){
					color21 = colorCenter;
				}
				try{
				color22 = img.getRGB(x+1, y+1);
				}catch(Exception e){
					color22 = colorCenter;
				}
				
				dst.setRGB(x, y, colorCenter);
				r1[0][0] = (color00 >> 16) & 0xff;
				g1[0][0] = (color00 >> 8) & 0xff;
				b1[0][0] = (color00 & 0xff);
				
				r1[0][1] = (color01 >> 16) & 0xff;
				g1[0][1] = (color01 >> 8) & 0xff;
				b1[0][1] = (color01 & 0xff);
				
				r1[0][2] = (color02 >> 16) & 0xff;
				g1[0][2] = (color02 >> 8) & 0xff;
				b1[0][2] = (color02 & 0xff);
				
				r1[1][0] = (color10 >> 16) & 0xff;
				g1[1][0] = (color10 >> 8) & 0xff;
				b1[1][0] = (color10 & 0xff);
				
				
				r1[1][1] = (color11 >> 16) & 0xff;
				g1[1][1] = (color11 >> 8) & 0xff;
				b1[1][1] = (color11 & 0xff);
				
				
				r1[1][2] = (color12 >> 16) & 0xff;
				g1[1][2] = (color12 >> 8) & 0xff;
				b1[1][2] = (color12 & 0xff);
				
				r1[2][0] = (color20 >> 16) & 0xff;
				g1[2][0] = (color20 >> 8) & 0xff;
				b1[2][0] = (color20 & 0xff);
				
				r1[2][1] = (color21 >> 16) & 0xff;
				g1[2][1] = (color21 >> 8) & 0xff;
				b1[2][1] = (color21 & 0xff);
				
				r1[2][2] = (color22 >> 16) & 0xff;
				g1[2][2] = (color22 >> 8) & 0xff;
				b1[2][2] = (color22 & 0xff);
				int rCount=0, gCount=0,bCount=0;
				int rLength=0,gLength=0,bLength=0;
				for(int i=0 ;i <=2 ; i++){
					for(int j=0 ; j<=2;j++){
						if(((255-r1[i][j])>160) && ((255-g1[i][j])>160) && ((255-b1[i][j])>160)){
						}else{
							rCount+=r1[i][j];
							gCount+=g1[i][j];
							bCount+=b1[i][j];
							rLength+=1;
							gLength+=1;
							bLength+=1;
						}
					}
				}
				if(rLength!=0){
					Color color = new Color(rCount/rLength, gCount/gLength, bCount/bLength);
					dst.setRGB(x, y, color.getRGB());
				}else{
					dst.setRGB(x, y, 0xffffff);
				}
			}
		}
		return dst;
	}
}
