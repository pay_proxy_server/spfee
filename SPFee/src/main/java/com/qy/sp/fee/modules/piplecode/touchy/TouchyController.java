package com.qy.sp.fee.modules.piplecode.touchy;

import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.modules.piplecode.mini.MiniService;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/piple")
public class TouchyController {

	@Resource
	private TouchyService touchyService;

	@RequestMapping(value = "/touchy/send" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String sendSmsSuccess(String orderId)
	{
		JSONObject json = new JSONObject();
		try
		{
			JSONObject object = new JSONObject();
			object.put("orderId", orderId);
			json = touchyService.processSuccessSMS(object);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return json.toString();
	}

	@RequestMapping(value = "/touchy/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(@RequestBody String requestBody)
	{
		String result = "error";
		try
		{
			requestBody = StringUtil.urlDecodeWithUtf8(requestBody);
			result = touchyService.processPaySuccess(requestBody);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

}
