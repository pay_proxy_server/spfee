package com.qy.sp.fee.dao.impl;

import org.springframework.stereotype.Component;

import com.qy.sp.fee.dao.MyBatisRepository;
import com.qy.sp.fee.dto.TSdkconfigMobileBase;

@Component @MyBatisRepository
public interface SdkconfigMobileBaseDao {
    int deleteByPrimaryKey(TSdkconfigMobileBase record);

    int insert(TSdkconfigMobileBase record);

    int insertSelective(TSdkconfigMobileBase record);

    TSdkconfigMobileBase selectByPrimaryKey(TSdkconfigMobileBase record);
    
   TSdkconfigMobileBase selectSelective(TSdkconfigMobileBase record);

    int updateByPrimaryKeySelective(TSdkconfigMobileBase record);

    int updateByPrimaryKey(TSdkconfigMobileBase record);
}