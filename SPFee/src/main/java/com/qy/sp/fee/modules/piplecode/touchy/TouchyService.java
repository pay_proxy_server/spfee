package com.qy.sp.fee.modules.piplecode.touchy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.qy.sp.fee.common.utils.Base64;
import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dto.TChannel;
import com.qy.sp.fee.dto.TChannelPiple;
import com.qy.sp.fee.dto.TChannelPipleKey;
import com.qy.sp.fee.dto.TOrder;
import com.qy.sp.fee.dto.TOrderExt;
import com.qy.sp.fee.dto.TPiple;
import com.qy.sp.fee.dto.TPipleProduct;
import com.qy.sp.fee.dto.TPipleProductKey;
import com.qy.sp.fee.dto.TProduct;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

@Service
public class TouchyService extends ChannelService {
	private final static String VERSION = "1.0.0";

	private Logger log = LoggerFactory.getLogger(TouchyService.class);

	private static Map<String,String> provinceMap = new ConcurrentHashMap<String, String>();

	@Override
	protected boolean isUseableTradeDayAndMonth() {
		return true;
	}

	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		log.info("TouchyService processGetSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String imsi = requestBody.optString("imsi");
		String imei = requestBody.optString("imei");
		String ipProvince = requestBody.optString("ipProvince");
		String extData = requestBody.optString("extData");
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey) || StringUtil.isEmpty(pipleId) || StringUtil.isEmptyString(imsi))
		{
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		}
		else
		{
			BaseChannelRequest req = new BaseChannelRequest();
			req.setApiKey(apiKey);
			req.setImsi(imsi);
			req.setProductCode(productCode);
			req.setMobile(mobile);
			req.setIpProvince(ipProvince);
			// 调用合法性校验
			BaseResult bResult = this.accessVerify(req,pipleId);
			// 返回不为空则校验不通过
			if(bResult!=null)
			{
				result.put("resultCode",bResult.getResultCode());
				result.put("resultMsg",bResult.getResultMsg());
				return result;
			}
			String groupId = KeyHelper.createKey();
			statistics(STEP_GET_SMS_CHANNEL_TO_PLATFORM, groupId, requestBody.toString());
			TChannel tChannel = tChannelDao.selectByApiKey(req.getApiKey());
			TProduct tProduct = tProductDao.selectByCode(req.getProductCode());
			TPipleProductKey ppkey = new TPipleProductKey();
			ppkey.setPipleId(pipleId);
			ppkey.setProductId(tProduct.getProductId());
			TPipleProduct pipleProduct = tPipleProductDao.selectByPrimaryKey(ppkey);
			TPiple piple = tPipleDao.selectByPrimaryKey(pipleId);
			//保存订单
			TouchyService.TouchyOrder order = new TouchyService.TouchyOrder();
			order.setOrderId(KeyHelper.getRandomNumber(16));
			order.setPipleId(pipleId);
			order.setChannelId(tChannel.getChannelId());
			order.setMobile(mobile);
			order.setProvinceId(req.getProvinceId());
			order.setImsi(imsi);
			order.setImei(imei);
			order.setProductId(tProduct.getProductId());
			order.setOrderStatus(GlobalConst.OrderStatus.INIT);
			order.setSubStatus(GlobalConst.SubStatus.PAY_INIT);
			order.setCreateTime(DateTimeUtils.getCurrentTime());
			order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
			order.setGroupId(groupId);
			order.setExtData(extData);
			try
			{
				SaveOrderInsert(order);
				result.put("orderId",order.getOrderId());
				JSONObject params = new JSONObject();
				params.put("consumercode", pipleProduct.getPipleProductCode());
				params.put("chid", piple.getPipleAuthD());
				params.put("imei", imei);
				params.put("imsi", imsi);
				params.put("provinceId", provinceMap.get(order.getProvinceId()+""));
				params.put("network", "CMWAP");
				params.put("cpparam", order.getOrderId());
				JSONObject json = new JSONObject();
				json.put("version", VERSION);
				json.put("msgID", "0");
				json.put("data", Base64.encodeBytes(params.toString().getBytes(),Base64.DONT_BREAK_LINES));
				log.info(" TouchyService reqUrl:"+  piple.getPipleUrlA()+";"+params.toString()+";"+json.toString());
				statistics(STEP_GET_SMS_PLATFORM_TO_BASE, groupId,piple.getPipleUrlA()+";"+params.toString()+";"+json.toString());
				String pipleResult = HttpClientUtils.doPost(piple.getPipleUrlA(), json.toString(), HttpClientUtils.UTF8);
				log.info(" TouchyService getPipleResult:"+  pipleResult);
				statistics(STEP_BACK_SMS_BASE_TO_PLATFORM, groupId,pipleResult);
				if(StringUtil.isNotEmptyString(pipleResult))
				{
					JSONObject jsonObj = JSONObject.fromObject(pipleResult);
					String errorCode = jsonObj.optString("error_code");
					if ("0".equals(errorCode))
					{
						String guid = jsonObj.optString("guid");
						String smsSend1 = jsonObj.optString("smsregport");
						String smsContent1 = new String(jsonObj.optString("smsreg"));
						String smsSend2 = jsonObj.optString("smsorderport");
						String smsContent2 = new String(jsonObj.optString("smsorder"));
						order.setSmsContent1(smsContent1);
						order.setSmsContent2(smsContent2);
						order.setSmsSend1(smsSend1);
						order.setSmsSend2(smsSend2);
						order.setPipleOrderId(guid);
						order.setResultCode(errorCode);
						result.put("smsSend1", smsSend1);
						result.put("smsContent1", smsContent1);
						result.put("smsSend2", smsSend2);
						result.put("smsContent2", smsContent2);
						result.put("resultCode", GlobalConst.Result.SUCCESS);
						result.put("resultMsg","请求成功。");

						order.setModTime(DateTimeUtils.getCurrentTime());
						order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
						order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_SUCCESS);
					}
					else
					{
						result.put("resultCode", GlobalConst.Result.ERROR);
						result.put("resultMsg","请求失败");
						order.setResultCode(errorCode);
						order.setModTime(DateTimeUtils.getCurrentTime());
						order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
						order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
					}
				}
				else
				{
					result.put("resultCode", GlobalConst.Result.ERROR);
					result.put("resultMsg","请求失败");
					order.setModTime(DateTimeUtils.getCurrentTime());
					order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
					order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("TouchyService :::: error------>" + e.toString());
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","接口异常");
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
			}
			SaveOrderUpdate(order);
			statistics(STEP_BACK_SMS_PLATFORM_TO_CHANNEL, groupId, result.toString());
			return result;
		}
	}

	public JSONObject processSuccessSMS(JSONObject requestBody) throws Exception
	{
		log.info("TouchyService processSuccessSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String orderId = requestBody.optString("orderId");
		TOrder order = tOrderDao.selectByPrimaryKey(orderId);
		TPiple piple = tPipleDao.selectByPrimaryKey(order.getPipleId());
		if(order != null && piple != null)
		{
			JSONObject params = new JSONObject();
			params.put("guid", order.getPipleOrderId());
			JSONObject json = new JSONObject();
			json.put("version", VERSION);
			json.put("msgID", "1");
			json.put("data", Base64.encodeBytes(params.toString().getBytes(),Base64.DONT_BREAK_LINES));
			log.info(" TouchyService reqUrl:" +  piple.getPipleUrlA() + ";" + params.toString()+";"+json.toString());
			statistics(STEP_SUBMIT_VCODE_CHANNEL_TO_PLATFORM, order.getGroupId(), piple.getPipleUrlA() + ";" + params.toString()+";"+json.toString());
			String pipleResult = HttpClientUtils.doPost(piple.getPipleUrlA(), json.toString(), "utf-8");
			log.info(" TouchyService getPipleResult:"+  pipleResult);
			statistics(STEP_SUBMIT_VCODE_PLARFORM_TO_BASE, order.getGroupId(), pipleResult);
			if(pipleResult != null && !"".equals(pipleResult))
			{
				JSONObject jsonObj = JSONObject.fromObject(pipleResult);
				if (jsonObj.has("error_code"))
				{
					String errorCode = jsonObj.optString("error_code");
					if ("0".equals(errorCode))
					{
//						String guid = jsonObj.optString("guid");
//						order.setPipleOrderId(guid);

						result.put("resultCode", GlobalConst.Result.SUCCESS);
						result.put("resultMsg","请求成功。");

						order.setModTime(DateTimeUtils.getCurrentTime());
						order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
						order.setSubStatus(GlobalConst.SubStatus.PAY_SEND_MESSAGE_SUCCESS);
					}
					else
					{
						result.put("resultCode", GlobalConst.Result.ERROR);
						result.put("resultMsg","请求失败:" + errorCode);
						order.setResultCode(errorCode);
						order.setModTime(DateTimeUtils.getCurrentTime());
						order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
						order.setSubStatus(GlobalConst.SubStatus.PAY_SEND_MESSAGE_FAIL);
					}
				}
				else
				{
					result.put("resultCode", GlobalConst.Result.ERROR);
					result.put("resultMsg","请求失败:" + pipleResult);
					order.setModTime(DateTimeUtils.getCurrentTime());
					order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
					order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
				}
			}
			else
			{
				result.put("resultCode", GlobalConst.Result.ERROR);
				result.put("resultMsg","请求失败");
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_FAIL);
			}
			SaveOrderUpdate(order);
			return result;
		}
		else
		{
			result.put("resultCode", GlobalConst.Result.ERROR);
			result.put("resultMsg","orderId参数有误");
			return result;
		}
	}

	public String processPaySuccess(String xmlContent) throws Exception
	{
		logger.info("TouchyService processPaySuccess 支付同步数据:"+xmlContent);
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject requestBody = (JSONObject) xmlSerializer.read(xmlContent);
		String error = "error";
		if(requestBody == null || "".equals(requestBody) || "{}".equals(requestBody.toString()))
			return error;
		String orderId = requestBody.optString("cpparam");
		String hRet = requestBody.optString("hRet");
		String status = requestBody.optString("status");
		if(StringUtil.isEmpty(orderId))
			return error;
		TOrder existOrder = tOrderDao.selectByPrimaryKey(orderId);
		TPiple piple = tPipleDao.selectByPrimaryKey(existOrder.getPipleId());
		boolean isSend = false; //是否同步
		if(existOrder != null && piple != null)
		{
			TChannelPipleKey pkey = new TChannelPipleKey();
			pkey.setChannelId(existOrder.getChannelId());
			pkey.setPipleId(existOrder.getPipleId());
			TChannelPiple cp = tChannelPipleDao.selectByPrimaryKey(pkey);
			if(cp == null)
				return "channel error";
			TProduct tProduct = this.tProductDao.selectByPrimaryKey(existOrder.getProductId());
			String productCode = tProduct.getProductCode();
			if(hRet.equals("0"))
			{
				if(GlobalConst.OrderStatus.SUCCESS != existOrder.getOrderStatus())
				{
					existOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					existOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
					existOrder.setModTime(DateTimeUtils.getCurrentTime());
					existOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					existOrder.setResultCode(status);

					JSONObject params = new JSONObject();
					params.put("guid", existOrder.getPipleOrderId());
					params.put("backdata", xmlContent);
					JSONObject json = new JSONObject();
					json.put("version", VERSION);
					json.put("msgID", "3");
					json.put("data", Base64.encodeBytes(params.toString().getBytes()));
					log.info(" TouchyService reqUrl:" +  piple.getPipleUrlA() + ";" + params.toString());
					statistics(STEP_BACK_VCODE_BASE_TO_PLATFORM, existOrder.getGroupId(), piple.getPipleUrlA() + ";" + params.toString());
					String pipleResult = HttpClientUtils.doPost(piple.getPipleUrlA(), json.toString(), "utf-8");
					log.info(" TouchyService getPipleResult:"+  pipleResult);
					statistics(STEP_BACK_VCODE_PLATFORM_TO_CHANNEL, existOrder.getGroupId(), pipleResult);
					doWhenPaySuccess(existOrder);
					boolean bDeducted = existOrder.deduct(cp.getVolt());
					if (!bDeducted)
						isSend = true;
				}
			}
			else
			{
				existOrder.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				existOrder.setSubStatus(GlobalConst.SubStatus.PAY_ERROR);
				existOrder.setModTime(DateTimeUtils.getCurrentTime());
				existOrder.setResultCode(status);

				JSONObject params = new JSONObject();
				params.put("guid", existOrder.getPipleOrderId());
				params.put("backdata", xmlContent);
				JSONObject json = new JSONObject();
				json.put("version", VERSION);
				json.put("msgID", "3");
				json.put("data", Base64.encodeBytes(params.toString().getBytes()));
				log.info(" TouchyService reqUrl:" +  piple.getPipleUrlA() + ";" + params.toString());
				statistics(STEP_BACK_VCODE_BASE_TO_PLATFORM, existOrder.getGroupId(), piple.getPipleUrlA() + ";" + params.toString());
				String pipleResult = HttpClientUtils.doPost(piple.getPipleUrlA(), json.toString(), "utf-8");
				log.info(" TouchyService getPipleResult:"+  pipleResult);
				statistics(STEP_BACK_VCODE_PLATFORM_TO_CHANNEL, existOrder.getGroupId(), pipleResult);
			}
			statistics(STEP_PAY_BASE_TO_PLATFORM, existOrder.getGroupId(), requestBody.toString());
			SaveOrderUpdate(existOrder);
			if(isSend)
				notifyChannel(cp.getNotifyUrl(), existOrder, productCode, "ok");
		}
		return "OK";
	}


	private class TouchyOrder extends TOrder{
		private String smsContent1;
		private String smsSend1;
		private String smsContent2;
		private String smsSend2;

		public String getSmsContent1() {
			return smsContent1;
		}

		public void setSmsContent1(String smsContent1) {
			this.smsContent1 = smsContent1;
		}

		public String getSmsSend1() {
			return smsSend1;
		}

		public void setSmsSend1(String smsSend1) {
			this.smsSend1 = smsSend1;
		}

		public String getSmsContent2() {
			return smsContent2;
		}

		public void setSmsContent2(String smsContent2) {
			this.smsContent2 = smsContent2;
		}

		public String getSmsSend2() {
			return smsSend2;
		}

		public void setSmsSend2(String smsSend2) {
			this.smsSend2 = smsSend2;
		}

		public List<TOrderExt> gettOrderExts()
		{
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.smsContent1 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent1");
				oExt.setExtValue(this.smsContent1);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsContent2 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent2");
				oExt.setExtValue(this.smsContent2);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend1 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend1");
				oExt.setExtValue(this.smsSend1);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend2 != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend2");
				oExt.setExtValue(this.smsSend2);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}

	static {
		provinceMap.put("0", "0");
		provinceMap.put("1", "100");
		provinceMap.put("2", "220");
		provinceMap.put("3", "311");
		provinceMap.put("4", "351");
		provinceMap.put("5", "471");
		provinceMap.put("6", "240");
		provinceMap.put("7", "431");
		provinceMap.put("8", "451");
		provinceMap.put("9", "210");
		provinceMap.put("10", "250");
		provinceMap.put("11", "571");
		provinceMap.put("12", "551");
		provinceMap.put("13", "591");
		provinceMap.put("14", "791");
		provinceMap.put("15", "531");
		provinceMap.put("16", "371");
		provinceMap.put("17", "270");
		provinceMap.put("18", "731");
		provinceMap.put("19", "200");
		provinceMap.put("20", "771");
		provinceMap.put("21", "898");
		provinceMap.put("22", "230");
		provinceMap.put("23", "280");
		provinceMap.put("24", "851");
		provinceMap.put("25", "871");
		provinceMap.put("26", "891");
		provinceMap.put("27", "290");
		provinceMap.put("28", "931");
		provinceMap.put("29", "971");
		provinceMap.put("30", "951");
		provinceMap.put("31", "991");

	}
}
