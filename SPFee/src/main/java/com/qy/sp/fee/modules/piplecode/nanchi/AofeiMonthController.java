package com.qy.sp.fee.modules.piplecode.nanchi;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qy.sp.fee.common.utils.StringUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class AofeiMonthController {

	@Resource
	private AofeiMonthService437 aofeiMonthService437;
	@Resource
	private AofeiMonthService441 aofeiMonthService441;
	@Resource
	private AofeiMonthService442 aofeiMonthService442;
	
	@RequestMapping(value = "/aofei/sync" ,produces = {"application/json;charset=UTF-8"})
	//LAOVIP1#webId
	@ResponseBody
	public String channelRequest(String userId,String serviceId,String webId,String area,String productId,String traceUniqueID,String timeStamp,String updateType){
		try{
			if(StringUtil.isEmpty(traceUniqueID)){
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("area", area);
				JSONObject result = null;
				if("802000192437".equals(serviceId)){//802000192437
					requestObj.put("productCode", "P01000");
					result = aofeiMonthService437.processGetSMS(requestObj);
				}else if("802000192441".equals(serviceId)){//802000192441
					requestObj.put("productCode", "P01500");
					result = aofeiMonthService441.processGetSMS(requestObj);
				}else if("802000192442".equals(serviceId)){//802000192442
					requestObj.put("productCode", "P02000");
					result = aofeiMonthService442.processGetSMS(requestObj);
				}
				String resultCode = result.optString("resultCode");
				if("0".equals(resultCode)){
					return "0000";
				}
			}else{
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("productId", productId);
				requestObj.put("traceUniqueId", traceUniqueID);
				requestObj.put("timeStamp", timeStamp);
				requestObj.put("updateType", updateType);
				String result = null;
				if("802000192437".equals(serviceId)){//802000192437
					result = aofeiMonthService437.processPaySuccess(requestObj);
				}else if("802000192441".equals(serviceId)){//802000192441
					result = aofeiMonthService441.processPaySuccess(requestObj);
				}else if("802000192442".equals(serviceId)){//802000192442
					result = aofeiMonthService442.processPaySuccess(requestObj);
				}
				if("ok".equals(result)){
					return "0000";
				}
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "1007";
	}
}
