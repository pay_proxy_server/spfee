package com.qy.sp.fee.modules.piplecode.jishun;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qy.sp.fee.common.utils.StringUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class JiShunMonthController {

	@Resource
	private JiShunMonthService813 jiShunMonthService813;
	@Resource
	private JiShunMonthService814 jiShunMonthService814;
	@Resource
	private JiShunMonthService816 jiShunMonthService816;
	@Resource
	private JiShunMonthService820 jiShunMonthService820;
	@Resource
	private JiShunMonthService822 jiShunMonthService822;
	@Resource
	private JiShunMonthService835 jiShunMonthService835;
	

	@RequestMapping(value = "/jishun/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(String userId,String serviceId,String webId,String area,String productId,String traceUniqueId,String timeStamp,String updateType,String validate){
		try{
			if(StringUtil.isEmpty(traceUniqueId)){
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("area", area);
				JSONObject result = null;
				if("802000062219".equals(serviceId)){//700220835000
					requestObj.put("productCode", "P01000");
					result = jiShunMonthService835.processGetSMS(requestObj);
				}else if("802000062094".equals(serviceId)){//700220814000
					requestObj.put("productCode", "P01000");
					result = jiShunMonthService814.processGetSMS(requestObj);
				}else if("802000062093".equals(serviceId)){//700220813000
					requestObj.put("productCode", "P01000");
					result = jiShunMonthService813.processGetSMS(requestObj);
				}else if("802000062096".equals(serviceId)){//700220816000
					requestObj.put("productCode", "P01500");
					result = jiShunMonthService816.processGetSMS(requestObj);
				}else if("802000062102".equals(serviceId)){//700220820000
					requestObj.put("productCode", "P01500");
					result = jiShunMonthService820.processGetSMS(requestObj);
				}else if("802000062118".equals(serviceId)){//700220822000
					requestObj.put("productCode", "P01500");
					result = jiShunMonthService822.processGetSMS(requestObj);
				}
				String resultCode = result.optString("resultCode");
				if("0".equals(resultCode)){
					return "0000";
				}
			}else{
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("productId", productId);
				requestObj.put("traceUniqueId", traceUniqueId);
				requestObj.put("timeStamp", timeStamp);
				requestObj.put("updateType", updateType);
				requestObj.put("validate", validate);
				String result = null;
				if("802000062219".equals(serviceId)){//700220835000
					result = jiShunMonthService835.processPaySuccess(requestObj);
				}else if("802000062094".equals(serviceId)){//700220814000
					result = jiShunMonthService814.processPaySuccess(requestObj);
				}else if("802000062093".equals(serviceId)){//700220813000
					result = jiShunMonthService813.processPaySuccess(requestObj);
				}else if("802000062096".equals(serviceId)){//700220816000
					result = jiShunMonthService816.processPaySuccess(requestObj);
				}else if("802000062102".equals(serviceId)){//700220820000
					result = jiShunMonthService820.processPaySuccess(requestObj);
				}else if("802000062118".equals(serviceId)){//700220822000
					result = jiShunMonthService822.processPaySuccess(requestObj);
				}
				if("ok".equals(result)){
					return "0000";
				}
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "1007";
	}
}
