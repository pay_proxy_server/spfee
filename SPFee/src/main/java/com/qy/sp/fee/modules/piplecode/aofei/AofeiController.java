package com.qy.sp.fee.modules.piplecode.aofei;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class AofeiController {

	@Resource
	private Aofei1Service aofei1Service;
	@Resource
	private Aofei2Service aofei2Service;
	@Resource
	private Aofei3Service aofei3Service;

	@RequestMapping(value = "/aofeibaoyue/check" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelVertify1(String serviceId, String webId,String userId,String area)
	{
		JSONObject result = new JSONObject();
		try
		{
			JSONObject object = new JSONObject();
			object.put("serviceId", serviceId);
			object.put("webId", webId);
			object.put("userId", userId);
			object.put("area", area);
			if("802000192437".equals(serviceId)){//802000192437
				result = aofei1Service.processVertifySMS(object);
			}else if("802000192441".equals(serviceId)){//802000192441
				result = aofei2Service.processVertifySMS(object);
			}else if("802000192442".equals(serviceId)){//802000192442
				result = aofei3Service.processVertifySMS(object);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result.getString("resultCode");
	}

	@RequestMapping(value = "/aofei1/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest1(String orderId, String extData, String DGFlag)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("orderId", orderId);
			object.put("extData", extData);
			object.put("DGFlag", DGFlag);
			aofei1Service.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}
	@RequestMapping(value = "/aofei2/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest2(String orderId, String extData, String DGFlag)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("orderId", orderId);
			object.put("extData", extData);
			object.put("DGFlag", DGFlag);
			aofei2Service.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}
	@RequestMapping(value = "/aofei3/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest3(String orderId, String extData, String DGFlag)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("orderId", orderId);
			object.put("extData", extData);
			object.put("DGFlag", DGFlag);
			aofei3Service.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}

}
