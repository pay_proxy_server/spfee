package com.qy.sp.fee.modules.piplecode.fish;

import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dto.*;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Fish1Service extends ChannelService {
	private final static String PIPLE_ID = "14809906465224059793964";
	private final static String PRODUCT_CODE = "P01000";
	private final static String SMS_CONTENT = "KPHY#47as";

	private final static String PAY_SUCCESS = "DELIVERED";
	private final static String PAY_FAIL = "2";

	private Logger log = LoggerFactory.getLogger(Fish1Service.class);

	@Override
	protected boolean isUseableTradeDayAndMonth() {
		return true;
	}

	@Override
	public String getPipleId() {
		return PIPLE_ID;
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		log.info("Fish1Service processGetSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String imsi = requestBody.optString("imsi");
		String imei = requestBody.optString("imei");
		String ipProvince = requestBody.optString("ipProvince");
		String extData = requestBody.optString("extData");
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey) || StringUtil.isEmpty(pipleId))
		{
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		}
		else
		{
			BaseChannelRequest req = new BaseChannelRequest();
			req.setApiKey(apiKey);
			req.setImsi(imsi);
			req.setProductCode(productCode);
			req.setMobile(mobile);
			req.setIpProvince(ipProvince);
			// 调用合法性校验
			BaseResult bResult = this.accessVerify(req,pipleId);
			// 返回不为空则校验不通过
			if(bResult!=null)
			{
				result.put("resultCode",bResult.getResultCode());
				result.put("resultMsg",bResult.getResultMsg());
				return result;
			}
			String groupId = KeyHelper.createKey();
			statistics(STEP_GET_SMS_CHANNEL_TO_PLATFORM, groupId, requestBody.toString());
			TChannel tChannel = tChannelDao.selectByApiKey(req.getApiKey());
			TProduct tProduct = tProductDao.selectByCode(req.getProductCode());
			TPipleProductKey ppkey = new TPipleProductKey();
			ppkey.setPipleId(pipleId);
			ppkey.setProductId(tProduct.getProductId());
			TPiple piple = tPipleDao.selectByPrimaryKey(pipleId);
			//保存订单
			Fish1Service.FishOrder order = new Fish1Service.FishOrder();
			SimpleDateFormat sf = new SimpleDateFormat(DateTimeUtils.yyyyMMdd2);
			String orderKey = KeyHelper.creatKey(6);
			String orderId = sf.format(new Date()) + orderKey;
			order.setOrderId(orderId);
			order.setPipleId(pipleId);
			order.setChannelId(tChannel.getChannelId());
			order.setMobile(mobile);
			order.setProvinceId(req.getProvinceId());
			order.setImsi(imsi);
			order.setImei(imei);
			order.setProductId(tProduct.getProductId());
			order.setOrderStatus(GlobalConst.OrderStatus.INIT);
			order.setSubStatus(GlobalConst.SubStatus.PAY_INIT);
			order.setCreateTime(DateTimeUtils.getCurrentTime());
			order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
			order.setGroupId(groupId);
			order.setExtData(extData);
			try
			{
				SaveOrderInsert(order);
				result.put("orderId",order.getOrderId());
				result.put("smsContent", SMS_CONTENT + orderKey);
				result.put("smsSend", piple.getPipleUrlA());
				result.put("resultCode", GlobalConst.Result.SUCCESS);
				result.put("resultMsg","请求成功。");

				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
				order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_SUCCESS);
				order.setSmsSend(piple.getPipleUrlA());
				order.setSmsContent(SMS_CONTENT + orderKey);
				SaveOrderUpdate(order);
				statistics(STEP_BACK_SMS_PLATFORM_TO_CHANNEL, groupId, result.toString());
				return result;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","请求失败，接口异常:"+e.getMessage());
				return result;
			}
		}
	}

	@Override
	public String processPaySuccess(JSONObject requestBody) throws Exception
	{
		logger.info("Fish1Service processPaySuccess 支付同步数据:"+requestBody);
		String error = "error";
		if(requestBody==null || "".equals(requestBody) || "{}".equals(requestBody.toString()))
			return error;

		String smsId = requestBody.optString("smsId");
		String state = requestBody.optString("state");
		String userId = requestBody.optString("userid");
		String moContent = requestBody.optString("moContent");
		SimpleDateFormat sf = new SimpleDateFormat(DateTimeUtils.yyyyMMdd2);
		String orderId = sf.format(new Date()) + moContent.substring(SMS_CONTENT.length(), moContent.length());
		if(StringUtil.isEmpty(orderId))
			return error;
		TOrder existOrder = tOrderDao.selectByPrimaryKey(orderId);
		boolean isSend = false; //是否同步
		if(existOrder != null)
		{
			TChannelPipleKey pkey = new TChannelPipleKey();
			pkey.setChannelId(existOrder.getChannelId());
			pkey.setPipleId(existOrder.getPipleId());
			TChannelPiple cp = tChannelPipleDao.selectByPrimaryKey(pkey);
			if(cp == null)
				return "channel error";
			TProduct tProduct = this.tProductDao.selectByPrimaryKey(existOrder.getProductId());
			String productCode = tProduct.getProductCode();
			String flag = "";
			if(PAY_SUCCESS.equals(state))
			{
				if(GlobalConst.OrderStatus.SUCCESS != existOrder.getOrderStatus())
				{
					existOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					existOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS_DG);
					existOrder.setModTime(DateTimeUtils.getCurrentTime());
					existOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					existOrder.setAmount(new BigDecimal(10));
					existOrder.setPipleOrderId(smsId);
					existOrder.setAppId(userId);
					doWhenPaySuccess(existOrder);
					boolean bDeducted = existOrder.deduct(cp.getVolt());
					flag = "1";
					if (!bDeducted)
						isSend = true;
				}
			}
			else
			{
				existOrder.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				existOrder.setSubStatus(GlobalConst.SubStatus.PAY_ERROR_TG);
				existOrder.setModTime(DateTimeUtils.getCurrentTime());
				existOrder.setResultCode(state);
				existOrder.setPipleOrderId(smsId);
				existOrder.setAppId(userId);
				if(!existOrder.getDecStatus().equals(GlobalConst.DEC_STATUS.DEDUCTED))
					isSend = true;
				flag = "2";
			}
			statistics(STEP_PAY_BASE_TO_PLATFORM, existOrder.getGroupId(), requestBody.toString());
			SaveOrderUpdate(existOrder);
			if(isSend)
				notifyChannel(cp.getNotifyUrl(), existOrder, productCode, flag);
		}
		return "OK";
	}

	public class FishOrder extends TOrder{
		private String smsContent;
		private String smsSend;
		private String userId;

		public String getSmsContent() {
			return smsContent;
		}

		public void setSmsContent(String smsContent) {
			this.smsContent = smsContent;
		}

		public String getSmsSend() {
			return smsSend;
		}

		public void setSmsSend(String smsSend) {
			this.smsSend = smsSend;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public List<TOrderExt> gettOrderExts()
		{
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.smsContent != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent");
				oExt.setExtValue(this.smsContent);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend");
				oExt.setExtValue(this.smsSend);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.userId != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("userId");
				oExt.setExtValue(this.userId);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}

}
