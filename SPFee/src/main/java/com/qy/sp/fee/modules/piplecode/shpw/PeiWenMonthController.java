package com.qy.sp.fee.modules.piplecode.shpw;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qy.sp.fee.common.utils.StringUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class PeiWenMonthController {

	@Resource
	private PeiWenMonthService934 pwMonthService934;
	@Resource
	private PeiWenMonthService935 pwMonthService935;
	@Resource
	private PeiWenMonthService936 pwMonthService936;

	@RequestMapping(value = "/shpw/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(String userId,String serviceId,String webId,String area,String spId,String traceUniqueId,String timeStamp,String updateType,String sign, String effectiveTime, String expiryTime){
		try{
			if(StringUtil.isEmpty(traceUniqueId)){
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("area", area);
				JSONObject result = null;
				if("802000060936".equals(serviceId)){
					requestObj.put("productCode", "P01000");
					result = pwMonthService936.processGetSMS(requestObj);
				}else if("802000060935".equals(serviceId)){
					requestObj.put("productCode", "P01000");
					result = pwMonthService935.processGetSMS(requestObj);
				}else if("802000060934".equals(serviceId)){
					requestObj.put("productCode", "P01000");
					result = pwMonthService934.processGetSMS(requestObj);
				}
				String resultCode = result.optString("resultCode");
				if("0".equals(resultCode)){
					return "0000";
				}
			}else{
				JSONObject requestObj = new JSONObject();
				requestObj.put("userId", userId);
				requestObj.put("webId", webId);
				requestObj.put("serviceId", serviceId);
				requestObj.put("spId", spId);
				requestObj.put("traceUniqueId", traceUniqueId);
				requestObj.put("timeStamp", timeStamp);
				requestObj.put("updateType", updateType);
				requestObj.put("sign", sign);
				String result = null;
				if("802000060936".equals(serviceId)){
					result = pwMonthService936.processPaySuccess(requestObj);
				}else if("802000060935".equals(serviceId)){
					result = pwMonthService935.processPaySuccess(requestObj);
				}else if("802000060934".equals(serviceId)){
					result = pwMonthService934.processPaySuccess(requestObj);
				}
				if("ok".equals(result)){
					return "0000";
				}
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return "1007";
	}
}
