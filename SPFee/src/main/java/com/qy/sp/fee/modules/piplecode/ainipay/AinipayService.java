package com.qy.sp.fee.modules.piplecode.ainipay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dto.TChannelPiple;
import com.qy.sp.fee.dto.TChannelPipleKey;
import com.qy.sp.fee.dto.TOrder;
import com.qy.sp.fee.dto.TOrderExt;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;

import net.sf.json.JSONObject;

@Service
public class AinipayService extends ChannelService {
	private final static String ORDER_DEST = "10658077696619";
	private final static String CMD_ID_4 = "YX,253836,6,7038,1812526,619005,620520005";
	private final static String CMD_ID_10 = "YX,264339,4,a1d9,1807926,615001,10031234567891234511";
	private final static String PIPLE_ID = "14787431867431987847994";
	private final static String PRODUCT_CODE_4 = "P00400";
	private final static String PRODUCT_CODE_10 = "P01000";
	private Logger log = LoggerFactory.getLogger(AinipayService.class);

	@Override
	public String getPipleId() {
		return PIPLE_ID;
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		log.info("AinipayService processGetSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String imsi = requestBody.optString("imsi");
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey) || StringUtil.isEmpty(pipleId) || StringUtil.isEmpty(productCode) || StringUtil.isEmpty(imsi))
		{
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		}
		else
		{
			BaseChannelRequest req = new BaseChannelRequest();
			req.setApiKey(apiKey);
			req.setImsi(imsi);
			req.setProductCode(productCode);
			req.setMobile(mobile);
			// 调用合法性校验
			BaseResult bResult = this.accessVerify(req,pipleId);
			// 返回不为空则校验不通过
			if(bResult!=null)
			{
				result.put("resultCode",bResult.getResultCode());
				result.put("resultMsg",bResult.getResultMsg());
				return result;
			}
			try
			{
				if(PRODUCT_CODE_4.equals(productCode))
				{
					result.put("cmdid", CMD_ID_4);
					result.put("orderdest", ORDER_DEST);
					result.put("resultCode", GlobalConst.Result.SUCCESS);
					result.put("resultMsg", "请求成功。");
				}
				else if(PRODUCT_CODE_10.equals(productCode))
				{
					result.put("cmdid", CMD_ID_10);
					result.put("orderdest", ORDER_DEST);
					result.put("resultCode", GlobalConst.Result.SUCCESS);
					result.put("resultMsg","请求成功。");
				}
				else
				{
					result.put("resultCode",GlobalConst.CheckResult.PRODUCTCODE_ERROR+"");
					result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.PRODUCTCODE_ERROR));
				}

				return result;

			}
			catch (Exception e)
			{
				e.printStackTrace();
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","请求失败，接口异常:"+e.getMessage());
				return result;
			}
		}
	}

	@Override
	public String processPaySuccess(JSONObject requestBody) throws Exception
	{
		logger.info("AinipayService processPaySuccess 支付同步数据:"+requestBody);
		String error = "error";
		if(requestBody==null || "".equals(requestBody) || "{}".equals(requestBody.toString())){
			return error;
		}
		String linkid = requestBody.optString("linkid");
		String mobile = requestBody.optString("mobile");
		String orderdest = requestBody.optString("orderdest");
		String cmdid = requestBody.optString("cmdid");
		if(StringUtil.isEmpty(linkid)){
			return error;
		}
		if(StringUtil.isEmpty(cmdid)){
			return error;
		}
		boolean isSend = false; //是否同步
		String notifyUrl = "";
		TOrder exitOrder = tOrderDao.selectByPipleOrderId(linkid);
		if(CMD_ID_4.equals(cmdid))
		{
			if(exitOrder == null)
			{
				AinipayService.AinipayOrder order = new AinipayService.AinipayOrder();
				String groupId = KeyHelper.createKey();
				order.setOrderId(KeyHelper.createKey());
				order.setPipleId(getPipleId());
				order.setMobile(mobile);
				order.setProductId("4");
				order.setAmount(new BigDecimal(4));
				order.setGroupId(groupId);
				order.setPipleOrderId(linkid);
				order.setOrderdest(orderdest);
				order.setCmdid(cmdid);
				order.setResultCode(GlobalConst.Result.SUCCESS);
				order.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
				order.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
				order.setCreateTime(DateTimeUtils.getCurrentTime());
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setCompleteTime(DateTimeUtils.getCurrentTime());
				List<TChannelPiple> cps =  tChannelPipleDao.getListByPipleId(getPipleId());
				TChannelPiple cp = null;
				if(cps != null && cps.size() >0)
					cp = cps.get(0);
				if(cp == null)
					return error;
				order.setChannelId(cp.getChannelId());
				notifyUrl = cp.getNotifyUrl();
				doWhenPaySuccess(order);
				boolean bDeducted  = order.deduct(cp.getVolt());
				if(!bDeducted)
					isSend =true;
				statistics(STEP_PAY_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
				exitOrder = order;
				SaveOrderInsert(order);
			}
			else
			{
				if(GlobalConst.OrderStatus.SUCCESS != exitOrder.getOrderStatus())
				{
					exitOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					exitOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
					exitOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					doWhenPaySuccess(exitOrder);
					if(exitOrder.getDecStatus() == GlobalConst.DEC_STATUS.UNDEDUCTED)
						isSend =true;
					TChannelPipleKey pkey = new TChannelPipleKey();
					pkey.setChannelId(exitOrder.getChannelId());
					pkey.setPipleId(exitOrder.getPipleId());
					TChannelPiple cp =  tChannelPipleDao.selectByPrimaryKey(pkey);
					if(cp != null)
						notifyUrl = cp.getNotifyUrl();
					SaveOrderUpdate(exitOrder);
					statistics(STEP_PAY_BASE_TO_PLATFORM, exitOrder.getGroupId(), requestBody.toString());
				}
			}
			if(isSend && StringUtil.isNotEmptyString(notifyUrl))
				notifyChannel(notifyUrl, exitOrder, PRODUCT_CODE_4, "ok");
		}
		else if(CMD_ID_10.equals(cmdid))
		{
			if(exitOrder == null)
			{
				AinipayService.AinipayOrder order = new AinipayService.AinipayOrder();
				String groupId = KeyHelper.createKey();
				order.setOrderId(KeyHelper.createKey());
				order.setPipleId(getPipleId());
				order.setMobile(mobile);
				order.setProductId("10");
				order.setAmount(new BigDecimal(10));
				order.setGroupId(groupId);
				order.setPipleOrderId(linkid);
				order.setOrderdest(orderdest);
				order.setCmdid(cmdid);
				order.setResultCode(GlobalConst.Result.SUCCESS);
				order.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
				order.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
				order.setCreateTime(DateTimeUtils.getCurrentTime());
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setCompleteTime(DateTimeUtils.getCurrentTime());
				List<TChannelPiple> cps =  tChannelPipleDao.getListByPipleId(getPipleId());
				TChannelPiple cp = null;
				if(cps != null && cps.size() >0)
					cp = cps.get(0);
				if(cp == null)
					return error;
				order.setChannelId(cp.getChannelId());
				notifyUrl = cp.getNotifyUrl();
				doWhenPaySuccess(order);
				boolean bDeducted  = order.deduct(cp.getVolt());
				if(!bDeducted)
					isSend =true;
				statistics(STEP_PAY_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
				exitOrder = order;
				SaveOrderInsert(order);
			}
			else
			{
				if(GlobalConst.OrderStatus.SUCCESS != exitOrder.getOrderStatus())
				{
					exitOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					exitOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
					exitOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					doWhenPaySuccess(exitOrder);
					if(exitOrder.getDecStatus() == GlobalConst.DEC_STATUS.UNDEDUCTED)
						isSend =true;
					TChannelPipleKey pkey = new TChannelPipleKey();
					pkey.setChannelId(exitOrder.getChannelId());
					pkey.setPipleId(exitOrder.getPipleId());
					TChannelPiple cp =  tChannelPipleDao.selectByPrimaryKey(pkey);
					if(cp != null)
						notifyUrl = cp.getNotifyUrl();
					SaveOrderUpdate(exitOrder);
					statistics(STEP_PAY_BASE_TO_PLATFORM, exitOrder.getGroupId(), requestBody.toString());
				}
			}
			if(isSend && StringUtil.isNotEmptyString(notifyUrl))
				notifyChannel(notifyUrl, exitOrder, PRODUCT_CODE_10, "ok");
		}
		else
		{
			if(exitOrder == null)
			{
				AinipayService.AinipayOrder order = new AinipayService.AinipayOrder();
				String groupId = KeyHelper.createKey();
				order.setOrderId(KeyHelper.createKey());
				order.setPipleId(getPipleId());
				order.setMobile(mobile);
				order.setProductId("10");
				order.setAmount(new BigDecimal(10));
				order.setGroupId(groupId);
				order.setPipleOrderId(linkid);
				order.setOrderdest(orderdest);
				order.setCmdid(cmdid);
				order.setResultCode(GlobalConst.Result.SUCCESS);
				order.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
				order.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
				order.setCreateTime(DateTimeUtils.getCurrentTime());
				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setCompleteTime(DateTimeUtils.getCurrentTime());
				statistics(STEP_PAY_BASE_TO_PLATFORM, order.getGroupId(), requestBody.toString());
				exitOrder = order;
				SaveOrderInsert(order);
			}
			else
			{
				if(GlobalConst.OrderStatus.SUCCESS != exitOrder.getOrderStatus())
				{
					exitOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					exitOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS);
					exitOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					doWhenPaySuccess(exitOrder);
					SaveOrderUpdate(exitOrder);
					statistics(STEP_PAY_BASE_TO_PLATFORM, exitOrder.getGroupId(), requestBody.toString());
				}
			}
		}
		return "OK";
	}


	public class AinipayOrder extends TOrder{
		private String orderdest; //短信端口号
		private String cmdid; //短信指令内容

		public String getOrderdest() {
			return orderdest;
		}

		public void setOrderdest(String orderdest) {
			this.orderdest = orderdest;
		}

		public String getCmdid() {
			return cmdid;
		}

		public void setCmdid(String cmdid) {
			this.cmdid = cmdid;
		}

		public List<TOrderExt> gettOrderExts()
		{
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.orderdest != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("orderdest");
				oExt.setExtValue(this.orderdest);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.cmdid != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("cmdid");
				oExt.setExtValue(this.cmdid);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}
}
