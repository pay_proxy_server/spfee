package com.qy.sp.fee.modules.piplecode.jishun;

import org.springframework.stereotype.Service;
@Service
public class JiShunMonthService822 extends JiShunMonthService{
	@Override
	public String getPipleId() {
		return "14741687774509388542806";
	}
	protected String getPrefix(){
		return "822_";
	}
}
