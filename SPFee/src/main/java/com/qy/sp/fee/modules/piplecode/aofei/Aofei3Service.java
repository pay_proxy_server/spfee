package com.qy.sp.fee.modules.piplecode.aofei;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.qy.sp.fee.common.utils.DateTimeUtils;
import com.qy.sp.fee.common.utils.GlobalConst;
import com.qy.sp.fee.common.utils.KeyHelper;
import com.qy.sp.fee.common.utils.StringUtil;
import com.qy.sp.fee.dto.TChannel;
import com.qy.sp.fee.dto.TChannelPiple;
import com.qy.sp.fee.dto.TChannelPipleKey;
import com.qy.sp.fee.dto.TOrder;
import com.qy.sp.fee.dto.TOrderExt;
import com.qy.sp.fee.dto.TPipleProductKey;
import com.qy.sp.fee.dto.TProduct;
import com.qy.sp.fee.entity.BaseChannelRequest;
import com.qy.sp.fee.entity.BaseResult;
import com.qy.sp.fee.modules.piplecode.base.ChannelService;

import net.sf.json.JSONObject;

@Service
public class Aofei3Service extends ChannelService {
	private final static String PIPLE_ID = "14802484595792989553646";

	private final static String CP_KEY = "3030";
	private final static String SMS_SEND = "1065889900";

	private static Map<String,String> provinceMap = new ConcurrentHashMap<String, String>();

	private Logger log = LoggerFactory.getLogger(Aofei3Service.class);

	@Override
	protected boolean isUseableTradeDayAndMonth() {
		return true;
	}

	@Override
	public String getPipleId() {
		return PIPLE_ID;
	}
	public String getPrefix(){
		return "ao3_";
	}
	@Override
	public JSONObject processGetSMS(JSONObject requestBody) throws Exception {
		log.info("Aofei3Service processGetSMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String productCode = requestBody.optString("productCode");
		String apiKey = requestBody.optString("apiKey");
		String mobile = requestBody.optString("mobile");
		String pipleId = requestBody.optString("pipleId");
		String imsi = requestBody.optString("imsi");
		String imei = requestBody.optString("imei");
		String ipProvince = requestBody.optString("ipProvince");
		String extData = requestBody.optString("extData");
		if(StringUtil.isEmptyString(productCode) || StringUtil.isEmptyString(apiKey)   || StringUtil.isEmpty(pipleId))
		{
			result.put("resultCode",GlobalConst.CheckResult.MUST_PARAM_ISNULL+"");
			result.put("resultMsg",GlobalConst.CheckResultDesc.message.get(GlobalConst.CheckResult.MUST_PARAM_ISNULL));
			return result;
		}
		else
		{
			BaseChannelRequest req = new BaseChannelRequest();
			req.setApiKey(apiKey);
			req.setImsi(imsi);
			req.setProductCode(productCode);
			req.setMobile(mobile);
			req.setIpProvince(ipProvince);
			// 调用合法性校验
			BaseResult bResult = this.accessVerify(req,pipleId);
			// 返回不为空则校验不通过
			if(bResult!=null)
			{
				result.put("resultCode",bResult.getResultCode());
				result.put("resultMsg",bResult.getResultMsg());
				return result;
			}
			String groupId = KeyHelper.createKey();
			statistics(STEP_GET_SMS_CHANNEL_TO_PLATFORM, groupId, requestBody.toString());
			TChannel tChannel = tChannelDao.selectByApiKey(req.getApiKey());
			TProduct tProduct = tProductDao.selectByCode(req.getProductCode());
			TPipleProductKey ppkey = new TPipleProductKey();
			ppkey.setPipleId(pipleId);
			ppkey.setProductId(tProduct.getProductId());
			//保存订单
			Aofei3Service.Aofei3Order order = new Aofei3Service.Aofei3Order();
			String random = KeyHelper.getRandomNumber(15);
			order.setOrderId(getPrefix()+random);
			order.setPipleId(pipleId);
			order.setChannelId(tChannel.getChannelId());
			order.setMobile(mobile);
			order.setProvinceId(req.getProvinceId());
			order.setImsi(imsi);
			order.setImei(imei);
			order.setProductId(tProduct.getProductId());
			order.setOrderStatus(GlobalConst.OrderStatus.INIT);
			order.setSubStatus(GlobalConst.SubStatus.PAY_INIT);
			order.setCreateTime(DateTimeUtils.getCurrentTime());
			order.setAmount(new BigDecimal(tProduct.getPrice()/100.0));
			order.setGroupId(groupId);
			order.setExtData(extData);
			try
			{
				SaveOrderInsert(order);
				result.put("orderId",order.getOrderId());
				result.put("smsContent", "LAOVIP3#"+CP_KEY+random);
				result.put("smsSend", SMS_SEND);
				result.put("resultCode", GlobalConst.Result.SUCCESS);
				result.put("resultMsg","请求成功。");

				order.setModTime(DateTimeUtils.getCurrentTime());
				order.setOrderStatus(GlobalConst.OrderStatus.TRADING);
				order.setSubStatus(GlobalConst.SubStatus.PAY_GET_SMS_SUCCESS);
				order.setSmsContent("LAOVIP3#"+CP_KEY+random);
				order.setSmsSend(SMS_SEND);
				SaveOrderUpdate(order);
				statistics(STEP_BACK_SMS_PLATFORM_TO_CHANNEL, groupId, result.toString());
				return result;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				result.put("resultCode",GlobalConst.Result.ERROR);
				result.put("resultMsg","请求失败，接口异常:"+e.getMessage());
				return result;
			}
		}
	}

	@Override
	public JSONObject processVertifySMS(JSONObject requestBody)
	{
		log.info("Aofei3Service processVertifySMS requestBody:"+requestBody);
		JSONObject result = new JSONObject();
		String webId = requestBody.optString("webId");
		String area = requestBody.optString("area");
		if(webId.length() <= 4 || "200".equals(webId))
		{
			result.put("resultCode","0000");
			return result;
		}
		else
		{
			String cpKey = webId.substring(0, 4);
			String orderId = getPrefix()+webId.substring(4, webId.length());
			TOrder order = tOrderDao.selectByPrimaryKey(orderId);
			String provinceId = "0";
			if(provinceMap.containsKey(area))
				provinceId = provinceMap.get(area);
			order.setProvinceId(Integer.parseInt(provinceId));
			SaveOrderUpdate(order);
			if(order != null && CP_KEY.equals(cpKey) && order.getPipleId().equals(PIPLE_ID))
			{
				result.put("resultCode","0000");
				statistics(STEP_BACK_VCODE_BASE_TO_PLATFORM, order.getGroupId(), "验证通过！"+requestBody);
				return result;
			}
			else
			{
				result.put("resultCode","1005");
				return result;
			}
		}
	}

	@Override
	public String processPaySuccess(JSONObject requestBody) throws Exception
	{
		logger.info("Aofei3Service processPaySuccess 支付同步数据:"+requestBody);
		String error = "error";
		if(requestBody==null || "".equals(requestBody) || "{}".equals(requestBody.toString())){
			return error;
		}
		String orderId = getPrefix()+requestBody.optString("extData");
		String pipleOrderId = requestBody.optString("orderId");
		String DGFlag = requestBody.optString("DGFlag");
		if(StringUtil.isEmpty(orderId)){
			return error;
		}
		TOrder existOrder = tOrderDao.selectByPrimaryKey(orderId);
		boolean isSend = false; //是否同步
		if(existOrder != null)
		{
			TChannelPipleKey pkey = new TChannelPipleKey();
			pkey.setChannelId(existOrder.getChannelId());
			pkey.setPipleId(existOrder.getPipleId());
			TChannelPiple cp = tChannelPipleDao.selectByPrimaryKey(pkey);
			if(cp == null)
				return "channel error";
			TProduct tProduct = this.tProductDao.selectByPrimaryKey(existOrder.getProductId());
			String productCode = tProduct.getProductCode();
			if(DGFlag.equals("1"))
			{
				if(GlobalConst.OrderStatus.SUCCESS != existOrder.getOrderStatus())
				{
					existOrder.setOrderStatus(GlobalConst.OrderStatus.SUCCESS);
					existOrder.setPipleOrderId(pipleOrderId);
					existOrder.setSubStatus(GlobalConst.SubStatus.PAY_SUCCESS_DG);
					existOrder.setModTime(DateTimeUtils.getCurrentTime());
					existOrder.setCompleteTime(DateTimeUtils.getCurrentTime());
					existOrder.setAmount(new BigDecimal(20));
					doWhenPaySuccess(existOrder);
					boolean bDeducted = existOrder.deduct(cp.getVolt());
					if (!bDeducted)
						isSend = true;
				}
			}
			else
			{
				existOrder.setOrderStatus(GlobalConst.OrderStatus.FAIL);
				existOrder.setSubStatus(GlobalConst.SubStatus.PAY_ERROR_TG);
				existOrder.setModTime(DateTimeUtils.getCurrentTime());
				existOrder.setResultCode(DGFlag);
				if(!existOrder.getDecStatus().equals(GlobalConst.DEC_STATUS.DEDUCTED))
					isSend = true;
			}
			statistics(STEP_PAY_BASE_TO_PLATFORM, existOrder.getGroupId(), requestBody.toString());
			SaveOrderUpdate(existOrder);
			if(isSend)
				notifyChannel(cp.getNotifyUrl(), existOrder, productCode, DGFlag);
		}
		return "OK";
	}


	private class Aofei3Order extends TOrder{
		private String smsContent;
		private String smsSend;

		public String getSmsContent() {
			return smsContent;
		}

		public void setSmsContent(String smsContent) {
			this.smsContent = smsContent;
		}

		public String getSmsSend() {
			return smsSend;
		}

		public void setSmsSend(String smsSend) {
			this.smsSend = smsSend;
		}

		public List<TOrderExt> gettOrderExts()
		{
			List<TOrderExt> tOrderExts = new ArrayList<TOrderExt>();
			if(this.smsContent != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsContent");
				oExt.setExtValue(this.smsContent);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			if(this.smsSend != null){
				TOrderExt oExt = new TOrderExt();
				oExt.setExtKey("smsSend");
				oExt.setExtValue(this.smsSend);
				oExt.setOrderId(this.getOrderId());
				tOrderExts.add(oExt);
			}
			return tOrderExts;
		}
	}

	static {
		provinceMap.put("230", "22");
		provinceMap.put("290", "27");
		provinceMap.put("351", "4");
		provinceMap.put("100", "1");
		provinceMap.put("551", "12");
		provinceMap.put("571", "11");
		provinceMap.put("311", "3");
		provinceMap.put("280", "23");
		provinceMap.put("951", "30");
		provinceMap.put("240", "6");
		provinceMap.put("871", "25");
		provinceMap.put("471", "5");
		provinceMap.put("851", "24");
		provinceMap.put("791", "14");
		provinceMap.put("898", "21");
		provinceMap.put("451", "8");
		provinceMap.put("591", "13");
		provinceMap.put("210", "9");
		provinceMap.put("200", "19");
		provinceMap.put("371", "16");
		provinceMap.put("971", "29");
		provinceMap.put("731", "18");
		provinceMap.put("531", "15");
		provinceMap.put("991", "31");
		provinceMap.put("771", "20");
		provinceMap.put("931", "28");
		provinceMap.put("431", "7");
		provinceMap.put("250", "10");
		provinceMap.put("270", "17");
		provinceMap.put("220", "2");
		provinceMap.put("891", "26");
	}
}
