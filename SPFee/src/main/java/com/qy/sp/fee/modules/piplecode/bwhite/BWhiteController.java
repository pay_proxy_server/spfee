package com.qy.sp.fee.modules.piplecode.bwhite;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qy.sp.fee.common.tools.TaskExecutor;
import com.qy.sp.fee.common.utils.StringUtil;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/piple")
public class BWhiteController {

	@Resource
	private BWhiteService bWhiteService;
	
	@RequestMapping(value = "/bwhite/checkcode" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String checkCode(@RequestBody String body){
		String hexString = StringUtil.reChange(body);
		try {
			byte bytes[] = StringUtil.hex2byte(hexString);
			BufferedImage img = ImageIO.read(new ByteInputStream(bytes, bytes.length));
			BufferedImage outs = BWhiteService.filterDeleteGrey(img);
			outs = BWhiteService.filterDeleteBlack(outs);
			ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();  
	        boolean flag = ImageIO.write(outs, "png", byteOutput);
	        if(flag){
		        bytes = byteOutput.toByteArray();  
				return bWhiteService.processCheckCode(bytes);
	        }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value = "/bwhite/syncorder" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public void syncOrder(@RequestBody final String reqBody){
		TaskExecutor.getInstance().execute(new Runnable() {
			
			@Override
			public void run() {
				try {
					if(StringUtil.isEmpty(reqBody))
						return ;
					JSONObject bodyObj = JSONObject.fromObject(reqBody);
					bWhiteService.updateOrderSuccess(bodyObj);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
