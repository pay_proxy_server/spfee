package com.qy.sp.fee.modules.piplecode.mini;

import com.qy.sp.fee.modules.piplecode.ainipay.AinipayService;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/piple")
public class MiniController {

	@Resource
	private MiniService miniService;
	@Resource
	private Mini1Service mini1Service;

	@RequestMapping(value = "/mini/sync" ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String channelRequest(String status, String phone, String cporderno, String time, String price, String td)
	{
		try
		{
			JSONObject object = new JSONObject();
			object.put("status", status);
			object.put("phone", phone);
			object.put("cporderno", cporderno);
			object.put("time", time);
			object.put("price", price);
			object.put("td", td);
			miniService.processPaySuccess(object);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "OK";
	}

}
