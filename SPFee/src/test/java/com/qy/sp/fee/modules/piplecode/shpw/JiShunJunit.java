package com.qy.sp.fee.modules.piplecode.shpw;
import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

public class JiShunJunit {
	String baseURL = "http://127.0.0.1:8001/SPFee";
//	String baseURL = "http://localhost:8080/SPFee";
	@Test
	public void testVertify(){
		String url = baseURL+"/piple/shpw/sync?serviceId=802000060936&userId=95651938917&webId="+StringUtil.urlEncodeWithUtf8("SEH10#1100312345612555551")+"&area=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testNotify(){
		String url = baseURL+"/piple/shpw/sync?serviceId=802000060936&spId=8101&effectiveTime=123&expiryTime=123&userId=95651938915&webId="+StringUtil.urlEncodeWithUtf8("SEH10#1100312345612555551")+"&area=1&traceUniqueId=1234567&timeStamp=123&updateType=1&sign=sign";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
