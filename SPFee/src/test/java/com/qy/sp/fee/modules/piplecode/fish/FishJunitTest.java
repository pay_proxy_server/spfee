package com.qy.sp.fee.modules.piplecode.fish;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;
import net.sf.json.JSONObject;
import org.junit.Test;

public class FishJunitTest {
	String baseURL = "http://114.55.144.136:8220/spfee";
	@Test
	public void testGetSMS(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("productCode", "P01000");
		parameters.put("apiKey", "1003");
		parameters.put("pipleId", "14809906465224059793964");
		parameters.put("extData", "123");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess(){
		String url = baseURL+"/piple/fish/sync"+"?smsId=1234567890&state=DELIVERED&moContent=" + StringUtil.urlEncodeWithUtf8("YZVIP#bn14810748131190863899920") + "&serviceCode=1065889900&userid=1359997115";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
