package com.qy.sp.fee.modules.piplecode.jishun;
import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

public class JiShunJunit {
//	String baseURL = "http://127.0.0.1:8001/SPFee";
	String baseURL = "http://114.55.144.136:8220/spfee";
	@Test
	public void testVertify(){
		String url = baseURL+"/piple/jishun/sync?serviceId=802000062094&userId=15651938915&webId="+StringUtil.urlEncodeWithUtf8("HZGZ#20@Q1003extData")+"&area=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testNotify(){
		String url = baseURL+"/piple/jishun/sync?serviceId=802000062094&userId=15651938915&webId="+StringUtil.urlEncodeWithUtf8("HZGZ#20@Q1003extData")+"&area=1&productId=123&traceUniqueId=1234567&timeStamp=123&updateType=1&validate=validate";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
