package com.qy.sp.fee.modules.piplecode.bwhite;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;

import com.qy.sp.fee.common.utils.FileUtils;
import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

import net.sf.json.JSONObject;

public class BWhiteJunit {
//	String baseURL = "http://192.168.1.7:8001/SPFee";
	String baseURL = "http://192.168.0.200:8001/SPFee";
//	String baseURL = "http://114.55.144.136:8220/spfee";
	@Test//获取验证码
	public void testGetSMS(){
			JSONObject parameters = new JSONObject();
			parameters.put("apiKey", "1003");
			parameters.put("mobile", "18313024197");
			parameters.put("pipleId", "14714136609207184697301");
			parameters.put("productCode", "P00100");
			parameters.put("pipleOrderId", "12345678");
			parameters.put("appId", "14744697768144546713074");
			parameters.put("contentId", "006123622000");
			parameters.put("releaseChannelId", "12064000");
			parameters.put("cpId", "799087");
			parameters.put("cid", "608716061924");
			parameters.put("appVersion", "4");
			parameters.put("ipProvince", "云南");
			
			try {
				String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
				System.out.println("result:\n"+result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	@Test//获取验证码
	public void testCheckCode(){
			try {
				byte[] bytes = FileUtils.InputStreamToByte(new FileInputStream("C:\\tesseract\\test\\0IB28FE0IL.jpg"));
				String result = HttpClientUtils.doPost(baseURL+"/piple/bwhite/checkcode", StringUtil.reChange(StringUtil.byte2hex(bytes)), HttpClientUtils.UTF8);
				System.out.println("result:\n"+result);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
	
	@Test
	public void testPicture(){
		try {
			File inputDir = new File("C:\\tessact\\input");
			File[] files = inputDir.listFiles();
			File outDir = new File("C:\\tessact\\output");
			for(File f : files){
				BufferedImage img = ImageIO.read(f);
				BufferedImage outs = BWhiteService.filterDeleteGrey(img);
				outs = BWhiteService.filterDeleteBlack(outs);
				ImageIO.write(outs, "png", new File(outDir,f.getName()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
