package com.qy.sp.fee.modules.piplecode.alipay;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

public class AliPayJunitTest {
	String baseURL = "http://192.168.1.200:8001/SPFee";
//	String baseURL = "http://139.196.27.18:8090/spfee";
//	String baseURL = "http://www.chinaunigame.net/spfee";
//	String baseURL = "http://123.56.158.156/spfee";
	@Test
	public void testGetOrder() throws Exception{
		 Map<String,String> param = new HashMap<String,String>();
		param.put("apiKey","1003");
        param.put("apiPwd","B97FED4E9994E33353DFAA8A31428E11BD7AE59");
        param.put("appId","a102");
        param.put("body","测试");
        param.put("subject","测试");
        param.put("price","0.1");
        String result = HttpClientUtils.doPost(baseURL+"/channel/alipay/order", param, HttpClientUtils.UTF8);
        System.out.println(result);
	}
	@Test
	public void testNotify() throws Exception{
		String requestUrl = baseURL+"/piple/alipay/notify"+"?discount=0.00&payment_type=1&subject="+StringUtil.urlEncodeWithUtf8("测试")+"&trade_no=2013082244524842&buyer_email="+StringUtil.urlEncodeWithUtf8("dlwdgl@gmail.com")+"&gmt_create="+StringUtil.urlEncodeWithUtf8("2013-08-22 14:45:23")+"&notify_type=trade_status_sync&quantity=1&out_trade_no=14709821225310375439248&seller_id=2088501624816263&notify_time="+StringUtil.urlEncodeWithUtf8("2013-08-22 14:45:24")+"&body="+StringUtil.urlEncodeWithUtf8("测试测试")+"&trade_status=TRADE_SUCCESS&is_total_fee_adjust=N&total_fee=1.00&gmt_payment="+StringUtil.urlEncodeWithUtf8("2013-08-22 14:45:24")+"&seller_email="+StringUtil.urlEncodeWithUtf8("xxx@alipay.com")+"&price=1.00&buyer_id=2088602315385429&notify_id=64ce1b6ab92d00ede0ee56ade98fdf2f4c&use_coupon=N&sign_type=RSA&sign=1glihU9DPWee+UJ82u3+mw3Bdnr9u01at0M/xJnPsGuHh+JA5bk3zbWaoWhU6GmLab3dIM4JNdktTcEUI9/FBGhgfLO39BKX/eBCFQ3bXAmIZn4l26fiwoO613BptT44GTEtnPiQ6+tnLsGlVSrFZaLB9FVhrGfipH2SWJcnwYs=";
		String result = HttpClientUtils.doGet(requestUrl,HttpClientUtils.UTF8);
		System.out.println(result);
	}
}
