package com.qy.sp.fee.modules.piplecode.touchy;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import net.sf.json.JSONObject;
import org.junit.Test;

public class TouchyJunitTest {
	String baseURL = "http://127.0.0.1:8080";
	@Test
	public void testGetSMS(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("imei", "352343059415610");
		parameters.put("productCode", "P00010");
		parameters.put("apiKey", "1003");
		parameters.put("pipleId", "14820318887193807164431");
//		parameters.put("hostIp", "211.139.29.69");
		parameters.put("extData", "123");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSendSuccess(){
		String url = baseURL+"/piple/touchy/send"+"?orderId=5827523523131855";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess(){
		String response = "%3C%3Fxml+version=%221.0%22+encoding%3D%22UTF-8%22%3F%3E%3Crequest%3E%3CuserId%3E1818844353%3C%2FuserId%3E%3CcontentId%3E639316032690%3C%2FcontentId%3E%3CconsumeCode%3E006069513008%3C%2FconsumeCode%3E%3Ccpid%3E799393%3C%2Fcpid%3E%3ChRet%3E0%3C%2FhRet%3E%3Cstatus%3E1800%3C%2Fstatus%3E%3CversionId%3E01%3C%2FversionId%3E%3Ccpparam%3E6999510244978291%3C%2Fcpparam%3E%3CpackageID+%2F%3E%3CprovinceId%3E871%3C%2FprovinceId%3E%3CchannelId%3E42216002%3C%2FchannelId%3E%3Cprice%3E1000%3C%2Fprice%3E%3C%2Frequest%3E";

		try {
			String result = HttpClientUtils.doPost(baseURL+"/piple/touchy/sync", response, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
