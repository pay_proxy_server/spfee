package com.qy.sp.fee.modules.piplecode.Aofei;

import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;

import net.sf.json.JSONObject;

public class AofeiJunitTest {
	String baseURL = "http://114.55.144.136:8223/spfee";
//	String baseURL = "http://127.0.0.1:8080";
//	String baseURL = "http://192.168.0.200:8001/SPFee";
	@Test
	public void testGetSMS1(){
		JSONObject parameters = new JSONObject();
//		parameters.put("mobile", "18794654812");
		parameters.put("imsi", "460021251261945");
		parameters.put("productCode", "P01000");
		parameters.put("apiKey", "3031");
		parameters.put("pipleId", "14802383446017353896868");
		parameters.put("extData", "123");
		parameters.put("ipProvince", "江苏");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSMS2(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("productCode", "P01500");
		parameters.put("apiKey", "1003");
		parameters.put("pipleId", "14802483103966711564808");
		parameters.put("extData", "123");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testGetSMS3(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("productCode", "P02000");
		parameters.put("apiKey", "1003");
		parameters.put("pipleId", "14802484595792989553646");
		parameters.put("extData", "123");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testVertify(){
		String url = baseURL+"/piple/aofeibaoyue/check"+"?serviceId=802000192437&webId=3030751478740851282&userId=1848489847";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess1(){
		String url = baseURL+"/piple/aofei1/sync"+"?orderId=123321&extData=480297698936465&DGFlag=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess2(){
		String url = baseURL+"/piple/aofei2/sync"+"?orderId=123321&extData=373745356844645&DGFlag=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess3(){
		String url = baseURL+"/piple/aofei3/sync"+"?orderId=123321&extData=5030416109912551&DGFlag=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
