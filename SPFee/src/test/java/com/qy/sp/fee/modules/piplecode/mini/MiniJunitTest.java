package com.qy.sp.fee.modules.piplecode.mini;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;
import net.sf.json.JSONObject;
import org.junit.Test;

public class MiniJunitTest {
//	String baseURL = "http://127.0.0.1:8080";
	String baseURL = "http://114.55.144.136:8220/spfee";
	@Test
	public void testGetSMS(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("imei", "861844032450982");
		parameters.put("productCode", "P01500");
		parameters.put("apiKey", "1003");
		parameters.put("pipleId", "14811811411512783478731");
//		parameters.put("hostIp", "211.139.29.69");
		parameters.put("extData", "123");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPaySuccess(){
		String url = baseURL+"/piple/mini/sync"+"?phone=18313024197&status=0000&cporderno=14811885384968624466360";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
