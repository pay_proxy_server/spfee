package com.qy.sp.fee.modules.piplecode.ainipay;

import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

import net.sf.json.JSONObject;

public class AinipayJunitTest {
	String baseURL = "http://127.0.0.1:8080";
	@Test
	public void testGetSMS(){
		JSONObject parameters = new JSONObject();
		parameters.put("mobile", "18313024197");
		parameters.put("imsi", "460021251261945");
		parameters.put("productCode", "P00400");
		parameters.put("apiKey", "3024");
		parameters.put("pipleId", "14787431867431987847994");

		try {
			String result = HttpClientUtils.doPost(baseURL+"/channel/getSms", parameters.toString(), HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLFRDONotify(){
		String url = baseURL+"/piple/ainipay/sync"+"?mobile=13840300000&linkid=7456537test&orderdest=10658077696615&cmdid="+StringUtil.urlEncodeWithUtf8("YX,264339,4,a1d9,1807926,615001,10031234567891234511")+"&fee=";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println("result:\n"+result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
