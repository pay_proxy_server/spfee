package com.qy.sp.fee.modules.piplecode.nanchi;
import org.junit.Test;

import com.qy.sp.fee.common.utils.HttpClientUtils;
import com.qy.sp.fee.common.utils.StringUtil;

public class AofeiJunit {
//	String baseURL = "http://127.0.0.1:8001/SPFee";
	String baseURL = "http://121.40.204.28:8220/spfee";
//	String baseURL = "http://114.55.144.136:8223/spfee";
	@Test
	public void testVertify(){
		String url = baseURL+"/piple/aofei/sync?serviceId=802000192437&userId=15651938916&webId="+StringUtil.urlEncodeWithUtf8("1003extData")+"&area=1";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testNotify(){
		String url = baseURL+"/piple/aofei/sync?serviceId=802000192437&userId=15651938916&webId="+StringUtil.urlEncodeWithUtf8("1003extData")+"&area=1&productId=10&traceUniqueID=1234567&timeStamp=123&updateType=2";
		try {
			String result = HttpClientUtils.doGet(url, HttpClientUtils.UTF8);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
